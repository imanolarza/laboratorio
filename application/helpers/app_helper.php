<?php
if ( ! function_exists('estaConectado'))
{
    /**
     * Verifica si se inició sesión en el sistema
     * @param bool $redirect
     * @return
     */
    function estaConectado($redirect = true)
    {
        $conectado = get_instance()->session->userdata('conectado');
        if($redirect):
            if(!$conectado):
                redirect(base_url("inicio/ingresar"));
            endif;
        else:
            return ($conectado != null)?$conectado:false;
        endif;
    }
}

if ( ! function_exists('array2Object')){
    function array2Object($array){
        return json_decode(json_encode($array));
    }
}

if ( ! function_exists('puedeVer'))
{
    /**
     * Verifica si el usuario puede acceder a elementos en el módulo
     * @param int $modulo
     * @return bool
     */
    function puedeVer($modulo)
    {
        $moduloID = get_instance()->admin_model->get_modulo_id($modulo);
        $permisos = get_instance()->session->userdata('permisos');
        return (isset($permisos[$moduloID]));
    }
}

if ( ! function_exists('puedeLeer'))
{
    /**
     * Verifica si el usuario puede leer elementos en el módulo
     * @param int $moduloID
     * @return bool
     */
    function puedeLeer($moduloID)
    {
        $permisos[$moduloID]['leer'] = false;
        if(get_instance()->session->userdata('permisos')){
            $permisos = get_instance()->session->userdata('permisos');
        }
        return $permisos[$moduloID]['leer'];
    }
}

if ( ! function_exists('puedeCrear'))
{
    /**
     * Verifica si el usuario puede crear elementos en el módulo
     * @param int $moduloID
     * @return bool
     */
    function puedeCrear($moduloID)
    {
        $permisos[$moduloID]['crear'] = false;
        if (get_instance()->session->userdata('permisos')) {
            $permisos = get_instance()->session->userdata('permisos');
        }
        return $permisos[$moduloID]['crear'];
    }
}

if ( ! function_exists('puedeEditar'))
{
    /**
     * Verifica si el usuario puede editar elementos del módulo
     * @param int $moduloID
     * @return bool
     */
    function puedeEditar($moduloID)
    {
        $permisos[$moduloID]['editar'] = false;
        if (get_instance()->session->userdata('permisos')) {
            $permisos = get_instance()->session->userdata('permisos');
        }
        return $permisos[$moduloID]['editar'];
    }
}

if ( ! function_exists('puedeEliminar'))
{
    /**
     * Verifica si el usuario puede eliminar elementos del módulo
     * @param int $moduloID
     * @return bool
     */
    function puedeEliminar($moduloID)
    {
        $permisos[$moduloID]['eliminar'] = false;
        if (get_instance()->session->userdata('permisos')) {
            $permisos = get_instance()->session->userdata('permisos');
        }
        return $permisos[$moduloID]['eliminar'];
    }
}

if( ! function_exists('getSaludoUsuario')){
    function getSaludoUsuario($tplSaludo){
        return sprintf($tplSaludo,get_instance()->session->userdata('nombre'));
    }
}

if( ! function_exists('getUID')){
    function getUID(){
        return get_instance()->session->userdata('uid');
    }
}

if( ! function_exists('getTipoUsuario')){
    function getTipoUsuario($uid){
        $tipoUsuario = get_instance()->admin_model->get_tipo_usuario($uid);
        return $tipoUsuario;
    }
}

if( ! function_exists('formatoFechaDB')){
    function formatoFechaDB($strFecha){
        $r = date('Y-m-d');
        $arrFecha = explode('/',$strFecha);
        if(count($arrFecha) > 1){
            $arrFecha = array_reverse($arrFecha);
        }else{
            $arrFecha = explode('-',$strFecha);
            $arrFecha = array_reverse($arrFecha);
        }
        $r = implode('-',$arrFecha);
        return $r;
    }
}

if( ! function_exists('formatoFecha')){
    function formatoFecha($strFecha){
        $arrFecha = explode('-',$strFecha);
        $arrFecha = array_reverse($arrFecha);
        return implode('/',$arrFecha);
    }
}

if( ! function_exists('generarTag')){
    function generarTag($valor,$tipo = 'primary'){
        return "<span class=\"badge badge-$tipo mr-1\">$valor</span>";
    }
}
<?php
if ( ! defined('BASEPATH')) exit('No se permite el acceso directo a las p&aacute;ginas de este sitio.');
class Mailer {

   function  __construct() {
      // Por si se ejecuta en un servidor Windows
      require_once(str_replace("\\", "/", APPPATH).'libraries/PHPMailer/src/PHPMailer.php');
	   require_once(str_replace("\\", "/", APPPATH).'libraries/PHPMailer/src/POP3.php');
	   require_once(str_replace("\\", "/", APPPATH).'libraries/PHPMailer/src/SMTP.php');
   } // end Constructor

   function index($no_cache) {
   }  // end function

} 
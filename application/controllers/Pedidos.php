<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pedidos extends CI_Controller
{

    /*******************************************
     * COSAS POR HACER
     * Crear el controlador del módulo (grupo:lógica de negocio)
     * Crear las vistas del módulo (grupo: ui/ux)
     * Crear el modelo del módulo (grupo: modelo/base de datos)
     *******************************************/
    
    public $data = [];
    public $js = [];
    public $moduloID = 0;

    public function __construct(){
        parent::__construct();

        $this->load->model('pedidos_model');
        $this->load->model('menu_model');
        $this->load->model('productos_model');

        $this->moduloID                 = $this->admin_model->get_modulo_id('pedidos');
        $this->data['moduloID']         = $this->moduloID;
        $this->data['tituloModulo']     = 'Pedidos';
        $this->data['tiposProductos']   = unserialize(TIPOS_PRODUCTOS);
        $this->data['columnas']         = ['id','fecha','usuario','detalle','total','estado','acciones'];
        $this->data['itemsMenu']        = array2Object([ 'parent'=>'pedidos', 'active'=>'' ]);
        $this->js['ajax_url']           = base_url('pedidos/listar');
        $this->js['tipoUsuario']        = getTipoUsuario(getUID());
        //$this->data['modal']            = $this->load->view('app/inc/modal',null,true);

        estaConectado();
        if(!puedeVer('pedidos')){
            redirect(base_url('inicio'));
        }

    }

    public function index($desde=false,$hasta = false) {
        $this->data['esCantinero'] = false;
        if(getTipoUsuario(getUID()) !== 'ALUMNO' || getTipoUsuario(getUID()) !== 'PROFESOR'){
            $this->data['esCantinero'] = true;
            $this->data['pedidosTotal'] = $this->pedidos_model->get_pedidos_estado();
            $this->data['pedidosCancelados'] = $this->pedidos_model->get_pedidos_estado('C');
            $this->data['pedidosPendientes'] = $this->pedidos_model->get_pedidos_estado('P');;
        }
        if($desde && $hasta){
            $this->data['desde'] = $desde;
            $this->data['hasta'] = $hasta;
            $this->js['ajax_url'] = base_url("pedidos/listar/$desde/$hasta");
        }else{
            $this->valoresFechas();
        }
        $this->data['js'] = $this->load->view('app/js/pedidos',$this->js,true);
        $this->load->view('app/inc/header',$this->data);
        $this->load->view('app/inc/nav');
        $this->load->view('app/pedidos/index');
        $this->load->view('app/inc/footer');
    }

    function valoresFechas(){
        $desde = $hasta = date('d/m/Y');
        $this->data['desde'] = $desde;
        $this->data['hasta'] = $hasta;
    }

    public function listar($desde = false,$hasta = false){
        echo $this->pedidos_model->get_data_filtrada($desde,$hasta);
    }

    public function nuevo(){
        if($this->input->post()){
            $campos = $this->input->post();
            $arr_excluidos = [];
            $formData = [];
            foreach($campos as $nombre => $valor):
                if(!in_array($nombre,$arr_excluidos)):
                    switch($nombre):
                        default: $formData[$nombre]=$valor; break;
                    endswitch;
                endif;
            endforeach;
            $result = $this->pedidos_model->add($formData);
            if($result['status'] == true):
                $this->session->set_flashdata('tipo','success');
                $this->session->set_flashdata('mensaje','Pedido registrado exitosamente!');
            else:
                $this->session->set_flashdata('tipo','danger');
                $this->session->set_flashdata('mensaje','Ocurrió un error al registrar los datos!');
            endif;
            redirect(base_url('pedidos'));
        }else{
            redirect(base_url('pedidos'));
        }
    }

    public function ver($id){
        $this->data['id']=$id;
        $this->data['datosPedido'] = $this->pedidos_model->get($id);
        $this->data['datosProductos'] = $this->pedidos_model->get_productos_pedido($id);
        $this->data['js'] = $this->load->view('app/js/pedidos',$this->js,true);
        $this->load->view('app/inc/header',$this->data);
        $this->load->view('app/inc/nav');
        $this->load->view('app/pedidos/ver');
        $this->load->view('app/inc/footer');
    }

    public function cargar($mid = false){

        
        $this->data['menus']    = $this->menu_model->get_menus();
        $this->data['js']       = $this->load->view('app/js/pedidos',$this->js,true);
        $menusKeys              = array_keys($this->data['menus']);
        if($mid):
            $this->data['mid'] = $mid;
            $this->data['productos'] = $this->productos_model->get_productos();
            $this->data['productosMenu'] = $this->menu_model->get_productos_menu($mid);
        else: 
            $this->data['productos'] = $this->productos_model->get_productos();
            $this->data['productosMenu'] = $this->menu_model->get_productos_menu($menusKeys[0]);
        endif;
        $this->load->view('app/inc/header',$this->data);
        $this->load->view('app/inc/nav');
        $this->load->view('app/pedidos/nuevo');
        $this->load->view('app/inc/footer');
    }

    public function guardar(){
        if($this->input->post()){
            $productos      = $this->input->post('productos');
            $observacion    = $this->input->post('observacion');
            $detallesPedido = ($this->session->userdata('detallesPedido'))?$this->session->userdata('detallesPedido'):[];
            $totalPedido    = ($this->session->userdata('totalPedido'))?$this->session->userdata('totalPedido'):0;
            foreach($productos as $pid => $value ){
                $dataProducto = $this->productos_model->get($pid);
                $detallesPedido[$pid] = [
                    'cantidad'=>1,
                    'subtotal'=>$dataProducto->precio
                ];
                $totalPedido += $dataProducto->precio;
            }
            $this->session->set_userdata('detallesPedido'       ,$detallesPedido);
            $this->session->set_userdata('totalPedido'          ,$totalPedido);
            $this->session->set_userdata('observacionPedido'    ,$observacion);
            redirect($this->agent->referrer());
            
        }else{
            redirect($this->agent->referrer());
        }
    }

    public function confirmar($fechaPedido,$uid = false){

        $detallesPedido     = ($this->session->userdata('detallesPedido'))?$this->session->userdata('detallesPedido'):[];
        $totalPedido        = ($this->session->userdata('totalPedido'))?$this->session->userdata('totalPedido'):0;
        $observacionPedido  = ($this->session->userdata('observacionPedido'))?$this->session->userdata('observacionPedido'):'';
        // guardamos la cabecera del pedido
        $formData = [
            'fecha_pedido'  => $fechaPedido,
            'fecha_hora'    => date('Y-m-d H:i:s'),
            'monto'         => $totalPedido,
            'usuarios_id'   => ($uid)?$uid:getUID(),
            'observacion'   => $observacionPedido
        ];
        $q = $this->pedidos_model->add_cabecera($formData);
        if($q['status']):
            $pedidoID = $q['id'];
            $formItemData = [];
            // guardamos el detalle del pedido
            foreach($detallesPedido as $pid => $item):
                $formItemData = [
                    'pedidos_cab_id' => $pedidoID,
                    'producto_id' => $pid,
                    'cantidad' => $item['cantidad'],
                    'subtotal' => $item['subtotal']
                ];
                $this->pedidos_model->add_detalle($formItemData);
            endforeach;
            //limpiamos el pedido de la sesión
            $this->session->unset_userdata('detallesPedido');
            $this->session->unset_userdata('totalPedido');
            $this->session->unset_userdata('observacionPedido');

            $this->session->set_flashdata('tipo','success');
            $this->session->set_flashdata('mensaje','Pedido procesado!');
            redirect(base_url("pedidos"));
        else:
            $this->session->set_flashdata('tipo','danger');
            $this->session->set_flashdata('mensaje','No se pudo procesar el pedido!');
        endif;
        redirect($this->agent->referrer());
    }

    public function editarItem($id){
        if($this->input->post()){
            $campos = $this->input->post();
            $arr_excluidos = [];
            $formData = [];
            foreach($campos as $nombre => $valor):
                if(!in_array($nombre,$arr_excluidos)):
                    switch($nombre):
                        default: $formData[$nombre]=$valor; break;
                    endswitch;
                endif;
            endforeach;
            $result = $this->pedidos_model->edit($id,$formData);
            if($result['status'] == true):
                $this->session->set_flashdata('tipo','success');
                $this->session->set_flashdata('mensaje','Pedido modificado exitosamente!');
            else:
                $this->session->set_flashdata('tipo','danger');
                $this->session->set_flashdata('mensaje','Ocurrió un error al registrar los datos!');
            endif;
            redirect(base_url("pedidos"));
        }else{
            redirect(base_url("pedidos"));
        }
    }

    public function entregar($id){
        $this->pedidos_model->set_estado($id,'E');
        redirect($this->agent->referrer());
    }

    public function cancelar($id){
        $this->pedidos_model->set_estado($id,'C');
        redirect($this->agent->referrer());
    }

    public function eliminarItem($id){
        $detallesPedido = ($this->session->userdata('detallesPedido'))?$this->session->userdata('detallesPedido'):[];
        $nuevoDetallesPedido = [];
        $totalPedido = 0;
        foreach($detallesPedido as $pid => $value):
            if($id != $pid):
                $nuevoDetallesPedido[$pid] = $value;
                $totalPedido+=$value['subtotal'];
            endif;
        endforeach;
        $this->session->set_userdata('detallesPedido'   ,$nuevoDetallesPedido);
        $this->session->set_userdata('totalPedido'      ,$totalPedido);
        redirect($this->agent->referrer());
    }

    public function eliminar($id){
        $result=$this->pedidos_model->delete($id);
        if($result['status'] == true):
            $this->session->set_flashdata('tipo','success');
            $this->session->set_flashdata('mensaje','Pedido eliminado existosamente!');
        else:
            $this->session->set_flashdata('tipo','danger');
            $this->session->set_flashdata('mensaje','Ocurrió un error al eliminar los datos!');
        endif;
        redirect($this->agent->referrer());
    }

    public function demoArrayDiasSemana (){
        $arrDias = unserialize(DIAS_MENU);
        var_dump($arrDias);
        echo PHP_EOL;
        $diaSemana = date('w');
        var_dump($diaSemana);
        echo PHP_EOL;
        echo "hoy es para mi: ".$arrDias[$diaSemana-1];
    }

}
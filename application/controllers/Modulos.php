<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Modulos extends CI_Controller
{

    /*******************************************
     * COSAS POR HACER
     * Crear el controlador del módulo (grupo:lógica de negocio)
     * Crear las vistas del módulo (grupo: ui/ux)
     * Crear el modelo del módulo (grupo: modelo/base de datos)
     *******************************************/
    public $data = [];
    public $js = [];
    public $moduloID = 0;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('modulos_model');

        $this->moduloID                 = $this->admin_model->get_modulo_id('opciones');
        $this->data['moduloID']         = $this->moduloID;
        $this->data['tituloModulo']     = 'Módulos del sistema';
        $this->data['columnas']         = ['id','descripcion','acciones'];
        $this->data['itemsMenu']        = array2Object([ 'parent'=>'modulos', 'active'=>'' ]);
        $this->js['ajax_url']           = base_url('modulos/listar');
        //$this->data['modal']            = $this->load->view('app/inc/modal',null,true);
    }

    public function index() {
        if(puedeLeer($this->moduloID)){
            $this->data['js'] = $this->load->view('app/js/modulos',$this->js,true);
            $this->load->view('app/inc/header',$this->data);
            $this->load->view('app/inc/nav');
            $this->load->view('app/modulos/index');
            $this->load->view('app/inc/footer');
        }else{
            $mensaje = array('tipo'=>'aviso','mensaje'=>'No tiene permiso para realizar ésta operación!');
            $this->session->set_flashdata($mensaje);
            redirect($this->agent->referrer());
        }
    }

    public function listar(){
        echo $this->modulos_model->get_data();
    }

    public function nuevo(){
        if (puedeCrear($this->moduloID)) {
            if ($this->input->post()) {
                $campos = $this->input->post();
                $arr_excluidos = [];
                $formData = [];
                foreach ($campos as $nombre => $valor):
                    if (!in_array($nombre, $arr_excluidos)):
                        switch ($nombre):
                            default: $formData[$nombre]=$valor;
                break;
                endswitch;
                endif;
                endforeach;
                $result = $this->modulos_model->add($formData);
                if ($result['status'] == true):
                    $this->session->set_flashdata('tipo','success');
                    $this->session->set_flashdata('mensaje', 'Módulo registrado exitosamente!'); else:
                    $this->session->set_flashdata('tipo','danger');
                    $this->session->set_flashdata('mensaje', 'Ocurrió un error al registrar los datos!');
                endif;
                redirect(base_url('modulos'));
            } else {
                redirect(base_url('modulos'));
            }
        }else{
            $mensaje = array('tipo'=>'aviso','mensaje'=>'No tiene permiso para realizar ésta operación!');
            $this->session->set_flashdata($mensaje);
            redirect($this->agent->referrer());
        }
    }

    public function editar($id){
        $this->data['id']=$id;
        $this->data['datosItem'] = $this->modulos_model->get($id);
        $this->data['js'] = $this->load->view('app/js/modulos',$this->js,true);
        $this->load->view('app/inc/header',$this->data);
        $this->load->view('app/inc/nav');
        $this->load->view('app/modulos/editar');
        $this->load->view('app/inc/footer');
    }

    public function editarItem($id){
        if($this->input->post()){
            $campos = $this->input->post();
            $arr_excluidos = [];
            $formData = [];
            foreach($campos as $nombre => $valor):
                if(!in_array($nombre,$arr_excluidos)):
                    switch($nombre):
                        default: $formData[$nombre]=$valor; break;
                    endswitch;
                endif;
            endforeach;
            $result = $this->modulos_model->edit($id,$formData);
            if($result['status'] == true):
                $this->session->set_flashdata('tipo','success');
                $this->session->set_flashdata('mensaje','Módulo modificado exitosamente!');
            else:
                $this->session->set_flashdata('tipo','danger');
                $this->session->set_flashdata('mensaje','Ocurrió un error al registrar los datos!');
            endif;
            redirect(base_url("modulos"));
        }else{
            redirect(base_url("modulos"));
        }
    }

    public function activar($id){
        $this->modulos_model->set_estado($id,'S');
        redirect($this->agent->referrer());
    }

    public function desactivar($id){
        $this->modulos_model->set_estado($id,'N');
        redirect($this->agent->referrer());
    }

    public function eliminar($id){
        $result=$this->modulos_model->delete($id);
        if($result['status'] == true):
            $this->session->set_flashdata('tipo','success');
            $this->session->set_flashdata('mensaje','Módulo eliminado existosamente!');
        else:
            $this->session->set_flashdata('tipo','danger');
            $this->session->set_flashdata('mensaje','Ocurrió un error al eliminar los datos!');
        endif;
        redirect($this->agent->referrer());
    }

}
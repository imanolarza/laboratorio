<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Inicio extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('usuarios_model');
        $this->load->model('modulos_model');
        $this->load->model('tipos_usuarios_model');
        $this->load->model('email_model');
        $this->data['modal'] = $this->load->view('app/inc/modal',null,true);
        $this->data['itemsMenu'] = [ 'parent'=>'inicio', 'active'=>'' ];
    }

	public function index() {
        estaConectado();
        $this->data['tituloModulo'] = "Dashboard";
        $this->data['itemsMenu']['active'] = 'dashboard';
        $this->data['itemsMenu'] = array2Object($this->data['itemsMenu']);
        $this->load->view('app/inc/header',$this->data);
        $this->load->view('app/inc/nav');
        $this->load->view('app/home/index');
        $this->load->view('app/inc/footer');
    }

    public function ingresar(){
        $this->load->view('app/login');
    }

    public function recuperar(){
        $this->load->view('app/recuperar');
    }

    public function resetear($hash){
        $valido = $this->usuarios_model->validar_hash($hash);
        if($valido){
            $this->data['hash'] = $hash;
            $this->load->view('app/resetear',$this->data);
        }else{
            redirect(base_url('inicio/ingresar'));
        }
    }

    public function registro(){
        $this->load->view('app/registro');
    }

    public function salir(){
        session_destroy();
        $this->ingresar();
    }

    public function doRecuperar(){
        if($this->input->post()):
            $email = $this->input->post('login');
            if($this->usuarios_model->existe_email_usuario($email)){
                // ENVIAMOS LA NOTIFICACIÓN CON EL HASH PARA EL RESETEO
                $dest = $email;
                $urlHash = base_url('inicio/resetear/'.sha1($email));
                $data = array(
                    'name' 		=> NOTI_NAME,
                    'from' 		=> NOTI_FROM,
                    'subject' 	=> NOTI_SUBJECT." - Recuperar acceso",
                    'title' 	=> (NOTI_TITLE." - Notificación"),
                    'body'		=> 'Ingrese al siguiente link para resetear la contraseña de su cuenta',
                    'link' 	    => $urlHash
                );
                $body = $this->load->view('mail/tpl',$data,true);
                $data = json_decode(json_encode($data));
                $notificado = $this->email_model->send_mail($dest,$data,$body);
                $output = array('tipo'=>'success','msg'=>'Le enviamos un enlace a su email para actualizar la contraseña!');
                $this->session->set_flashdata($output);
                redirect(base_url('inicio/ingresar'));
            }else{
                $output = array('tipo'=>'warning','msg'=>'La dirección de correo no es válida!');
                $this->session->set_flashdata($output);
                redirect($this->agent->referrer());
            }
        else:
            $output = array('tipo'=>'warning','msg'=>'Operación no válida!');
            $this->session->set_flashdata($output);
            redirect(base_url('inicio/ingresar'));
        endif;
    }

    public function doResetear($hash){
        if($this->input->post('pass') && $this->input->post('pass_confirm')):
            $pass = $this->input->post('pass');
            $passConfirm = $this->input->post('pass_confirm');
            if($pass === $passConfirm):
                $formData = ['pass'=>sha1($pass)];
                $this->usuarios_model->update_pass($hash,$formData); 
                $output = array('tipo'=>'success','msg'=>'La contraseña se actualizó con éxito!');
                $this->session->set_flashdata($output);
                redirect(base_url('inicio/ingresar'));
            else:
                $output = array('tipo'=>'warning','msg'=>'Las contraseñas no coinciden!');
                $this->session->set_flashdata($output);
                redirect($this->agent->referrer());
            endif;
        else:
            $output = array('tipo'=>'warning','msg'=>'Operación no válida!');
            $this->session->set_flashdata($output);
            redirect(base_url('inicio/ingresar'));
        endif;
    }

    public function doIngresar(){
        $login = $this->input->post('login');
        $clave = $this->input->post('pass');
        $resultado = $this->usuarios_model->validar_usuario($login,$clave);
        if($resultado['status']){
            $usuario = $resultado['datos'];
            $permisosUsuario = $this->usuarios_model->get_permisos($usuario->id);
            $datos_session = array(
                'conectado' => true,
                'uid'       => $usuario->id,
                'login'     => $usuario->login,
                'nombre'    => $usuario->nombre,
                'apellido'  => $usuario->apellido,
                'email'     => $usuario->email,
                'permisos'  => $permisosUsuario
            );
            $this->session->set_userdata($datos_session);
            redirect(base_url('usuarios'));
        }else{
            $output = array('tipo'=>'warning','msg'=>'Los datos no son correctos!');
            $this->session->set_flashdata($output);
            redirect(base_url('inicio/ingresar'));
        }
    }

    public function doRegistro(){
        $campos = $this->input->post();
        $arr_excluidos = ['pass_confirm'];
        $formData = ['tipos_usuarios_id'=>DEFAULT_TIPO_USUARIO];
        foreach($campos as $nombre => $valor):
            if(!in_array($nombre,$arr_excluidos)):
                switch($nombre):
                    case 'pass' : $formData[$nombre]=sha1($valor); break;
                    default     : $formData[$nombre]=$valor; break;
                endswitch;
            endif;
        endforeach;
        $result = $this->usuarios_model->add($formData);
        $output = array('tipo'=>'danger','msg'=>'Ocurrió un error al registrar los datos!');
        if($result['status']):
            $uid = $result['id'];
            // GENERAMOS LOS PERMISOS POR DEFAULT DEL USUARIO POR TIPO
            $modulos = $this->modulos_model->get_modulos();
            $tipo_usuario = $this->tipos_usuarios_model->get_tipo_usuario($formData['tipos_usuarios_id']);
            foreach($modulos as $modulo => $v):
                switch(strtolower($tipo_usuario)){
                    case 'alumno'   : $dataPermisos = unserialize(PERMISOS_CLIENTE); break;
                    case 'profesor' : $dataPermisos = unserialize(PERMISOS_CLIENTE); break;
                    default         : $dataPermisos = unserialize(PERMISOS_CANTINERO); break;
                }
                $dataPermisos['modulos_id'] = $modulo;
                $dataPermisos['usuarios_id'] = $uid;
                $this->usuarios_model->set_permisos_modulo($dataPermisos);
            endforeach;
            $output = array('tipo'=>'success','msg'=>'Registro exitoso!');
        endif;
        $this->session->set_flashdata($output);
        redirect ('inicio/ingresar');
    }

    public function doHash(){
        echo sha1('333@mail.com');
    }

}

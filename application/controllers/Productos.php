<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Productos extends CI_Controller
{

    /*******************************************
     * COSAS POR HACER
     * Crear el controlador del módulo (grupo:lógica de negocio)
     * Crear las vistas del módulo (grupo: ui/ux)
     * Crear el modelo del módulo (grupo: modelo/base de datos)
     *******************************************/
    public $data = [];
    public $js = [];
    public $moduloID = 0;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('productos_model');
        $this->load->model('upload_model');

        // guarnición, plato principal, especial

        $this->moduloID                 = $this->admin_model->get_modulo_id('pedidos');
        $this->data['moduloID']         = $this->moduloID;
        $this->data['tituloModulo']     = 'Productos del sistema';
        $this->data['columnas']         = ['id','descripcion','tipo','precio','acciones'];
        $this->data['itemsMenu']        = array2Object([ 'parent'=>'productos', 'active'=>'' ]);
        $this->data['tiposProductos']   = unserialize(TIPOS_PRODUCTOS);
        $this->js['ajax_url']           = base_url('productos/listar');
        //$this->data['modal']            = $this->load->view('app/inc/modal',null,true);
    }

    public function index() {
        if(puedeLeer($this->moduloID)){
            $this->data['js'] = $this->load->view('app/js/productos',$this->js,true);
            $this->load->view('app/inc/header',$this->data);
            $this->load->view('app/inc/nav');
            $this->load->view('app/productos/index');
            $this->load->view('app/inc/footer');
        }else{
            $mensaje = array('tipo'=>'aviso','mensaje'=>'No tiene permiso para realizar ésta operación!');
            $this->session->set_flashdata($mensaje);
            redirect($this->agent->referrer());
        }
    }

    public function listar(){
        echo $this->productos_model->get_data();
    }

    public function nuevo(){
        if (puedeCrear($this->moduloID)) {
            if ($this->input->post()) {
                $campos = $this->input->post();
                $arr_excluidos = [];
                $formData = [];
                foreach ($campos as $nombre => $valor):
                    if (!in_array($nombre, $arr_excluidos)):
                        switch ($nombre):
                            default: $formData[$nombre]=$valor;
                            break;
                        endswitch;
                    endif;
                endforeach;

                // PROCESAMOS ARCHIVOS
                foreach($_FILES as $fieldName => $file){
                    $uploadInfo = $this->upload_model->subir_img_producto($file,'productos');
                    if($uploadInfo['status']){
                        $formData[$fieldName] = $uploadInfo['file'];   
                    }
                }

                $result = $this->productos_model->add($formData);
                if ($result['status'] == true):
                    $this->session->set_flashdata('tipo','success');
                    $this->session->set_flashdata('mensaje', 'Producto registrado exitosamente!'); else:
                    $this->session->set_flashdata('tipo','danger');
                    $this->session->set_flashdata('mensaje', 'Ocurrió un error al registrar los datos!');
                endif;
                redirect(base_url('productos'));
            } else {
                redirect(base_url('productos'));
            }
        }else{
            $mensaje = array('tipo'=>'aviso','mensaje'=>'No tiene permiso para realizar ésta operación!');
            $this->session->set_flashdata($mensaje);
            redirect($this->agent->referrer());
        }
    }

    public function editar($id){
        $this->data['id']=$id;
        $this->data['datosItem'] = $this->productos_model->get($id);
        $this->data['js'] = $this->load->view('app/js/productos',$this->js,true);
        $this->load->view('app/inc/header',$this->data);
        $this->load->view('app/inc/nav');
        $this->load->view('app/productos/editar');
        $this->load->view('app/inc/footer');
    }

    public function editarItem($id){
        if($this->input->post()){
            $campos = $this->input->post();
            $arr_excluidos = [];
            $formData = [];
            foreach($campos as $nombre => $valor):
                if(!in_array($nombre,$arr_excluidos)):
                    switch($nombre):
                        default: $formData[$nombre]=$valor; break;
                    endswitch;
                endif;
            endforeach;

            // PROCESAMOS ARCHIVOS
            
            foreach($_FILES as $fieldName => $file){
                if( strlen($file['name']) > 0){
                    // eliminamos el archivo actual
                    $currentFileName = $this->productos_model->get_file_name($fieldName,$id);
                    $this->upload_model->eliminar_img($currentFileName,'productos');
                    // procesamos el nuevo archivo
                    $uploadInfo = $this->upload_model->subir_img_producto($file,'productos');
                    if($uploadInfo['status']){
                         $formData[$fieldName] = $uploadInfo['file'];   
                    }
                }
            }

            $result = $this->productos_model->edit($id,$formData);
            if($result['status'] == true):
                $this->session->set_flashdata('tipo','success');
                $this->session->set_flashdata('mensaje','Producto modificado exitosamente!');
            else:
                $this->session->set_flashdata('tipo','danger');
                $this->session->set_flashdata('mensaje','Ocurrió un error al registrar los datos!');
            endif;
            redirect(base_url("productos"));
        }else{
            redirect(base_url("productos"));
        }
    }

    public function activar($id){
        $this->productos_model->set_estado($id,'S');
        redirect($this->agent->referrer());
    }

    public function desactivar($id){
        $this->productos_model->set_estado($id,'N');
        redirect($this->agent->referrer());
    }

    public function eliminar($id){
        // Eliminamos los archivos asociados
        for ($i = 1; $i <= 2; $i++) {
            $currentFileName = $this->productos_model->get_file_name("imagen_$i", $id);
            $this->upload_model->eliminar_img($currentFileName, 'productos');
        }
        $result=$this->productos_model->delete($id);
        if($result['status'] == true):
            $this->session->set_flashdata('tipo','success');
            $this->session->set_flashdata('mensaje','Producto eliminado existosamente!');
        else:
            $this->session->set_flashdata('tipo','danger');
            $this->session->set_flashdata('mensaje','Ocurrió un error al eliminar los datos!');
        endif;
        redirect($this->agent->referrer());
    }

    public function eliminarArchivo($nroImagen,$pid){
        // eliminamos el archivo actual
        $currentFileName = $this->productos_model->get_file_name("imagen_$nroImagen",$pid);
        $this->upload_model->eliminar_img($currentFileName,'productos');
        $formData = ["imagen_$nroImagen"=>null];
        $this->productos_model->edit($pid,$formData);
        redirect($this->agent->referrer());
    }

}
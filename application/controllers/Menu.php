<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Menu extends CI_Controller
{

    /*******************************************
     * COSAS POR HACER
     * Crear el controlador del módulo (grupo:lógica de negocio)
     * Crear las vistas del módulo (grupo: ui/ux)
     * Crear el modelo del módulo (grupo: modelo/base de datos)
     *******************************************/
    public $data = [];
    public $js = [];
    public $moduloID = 0;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('menu_model');
        $this->load->model('productos_model');

        $this->moduloID                 = $this->admin_model->get_modulo_id('pedidos');
        $this->data['moduloID']         = $this->moduloID;
        $this->data['tituloModulo']     = 'Menu';
        $this->data['columnas']         = ['id','descripcion','dias','acciones'];
        $this->data['tiposProductos']   = unserialize(TIPOS_PRODUCTOS);
        $this->data['itemsMenu']        = array2Object([ 'parent'=>'menu', 'active'=>'' ]);
        $this->js['ajax_url']           = base_url('menu/listar');
        //$this->data['modal']            = $this->load->view('app/inc/modal',null,true);

        estaConectado();
        if(!puedeVer('pedidos')){
            redirect(base_url('inicio'));
        }

    }

    public function index() {
        $this->data['productos'] = $this->productos_model->get_productos();
        $this->data['js'] = $this->load->view('app/js/menu',$this->js,true);
        $this->load->view('app/inc/header',$this->data);
        $this->load->view('app/inc/nav');
        $this->load->view('app/menu/index');
        $this->load->view('app/inc/footer');
    }

    public function listar(){
        echo $this->menu_model->get_data();
    }

    public function nuevo(){
        if($this->input->post()){
            $campos = $this->input->post();
            $arr_excluidos = ['productos'];
            $formData = [
                'fecha_hora'    => date('Y-m-d H:i:s'),
                'uid'           => getUID()
            ];
            foreach($campos as $nombre => $valor):
                if(!in_array($nombre,$arr_excluidos)):
                    switch($nombre):
                        case 'lu'   : 
                        case 'ma'   : 
                        case 'mi'   : 
                        case 'ju'   : 
                        case 'vi'   : $formData[$nombre] = 'S'; break;
                        default     : $formData[$nombre]=$valor; break;
                    endswitch;
                endif;
            endforeach;
            $result = $this->menu_model->add($formData);
            if($result['status']):
                foreach($this->input->post('productos') as $pid => $value){
                    $this->menu_model->add_producto_menu($result['id'],$pid);
                }
                $this->session->set_flashdata('tipo','success');
                $this->session->set_flashdata('mensaje','Menú registrado exitosamente!');
            else:
                $this->session->set_flashdata('tipo','danger');
                $this->session->set_flashdata('mensaje','Ocurrió un error al registrar los datos!');
            endif;
            redirect(base_url('menu'));
        }else{
            redirect(base_url('menu'));
        }
    }

    public function editar($id){
        $this->data['id']=$id;
        $this->data['productos'] = $this->productos_model->get_productos();
        $this->data['datosItem'] = $this->menu_model->get($id);
        $this->data['productosMenu'] = $this->menu_model->get_productos_menu($id);
        $this->data['js'] = $this->load->view('app/js/menu',$this->js,true);
        $this->load->view('app/inc/header',$this->data);
        $this->load->view('app/inc/nav');
        $this->load->view('app/menu/editar');
        $this->load->view('app/inc/footer');
    }

    public function editarItem($id){
        if($this->input->post()){
            $campos = $this->input->post();
            $arr_excluidos = ['productos'];
            $formData = [    
                'lu'            => 'N',
                'ma'            => 'N',
                'mi'            => 'N',
                'ju'            => 'N',
                'vi'            => 'N'
            ];
            foreach($campos as $nombre => $valor):
                if(!in_array($nombre,$arr_excluidos)):
                    switch($nombre):
                        case 'lu'   : 
                        case 'ma'   : 
                        case 'mi'   : 
                        case 'ju'   : 
                        case 'vi'   : $formData[$nombre] = 'S'; break;
                        default     : $formData[$nombre]=$valor; break;
                    endswitch;
                endif;
            endforeach;
            $result = $this->menu_model->edit($id,$formData);
            if($result['status']):
                $this->menu_model->delete_productos_menu($id);
                foreach($this->input->post('productos') as $pid => $value){
                    $this->menu_model->add_producto_menu($id,$pid);
                }
                $this->session->set_flashdata('tipo','success');
                $this->session->set_flashdata('mensaje','Menú modificado exitosamente!');
            else:
                $this->session->set_flashdata('tipo','danger');
                $this->session->set_flashdata('mensaje','Ocurrió un error al registrar los datos!');
            endif;
            redirect(base_url("menu"));
        }else{
            redirect(base_url("menu"));
        }
    }

    public function activar($id){
        $this->menu_model->set_estado($id,'S');
        redirect($this->agent->referrer());
    }

    public function desactivar($id){
        $this->menu_model->set_estado($id,'N');
        redirect($this->agent->referrer());
    }

    public function eliminar($id){
        $result=$this->menu_model->delete($id);
        if($result['status'] == true):
            $this->session->set_flashdata('tipo','success');
            $this->session->set_flashdata('mensaje','Menú eliminado existosamente!');
        else:
            $this->session->set_flashdata('tipo','danger');
            $this->session->set_flashdata('mensaje','Ocurrió un error al eliminar los datos!');
        endif;
        redirect($this->agent->referrer());
    }

}
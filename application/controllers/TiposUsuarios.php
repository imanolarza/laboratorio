<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class TiposUsuarios extends CI_Controller
{

    /*******************************************
     * COSAS POR HACER
     * Crear el controlador del módulo (grupo:lógica de negocio)
     * Crear las vistas del módulo (grupo: ui/ux)
     * Crear el modelo del módulo (grupo: modelo/base de datos)
     *******************************************/
    public $data = [];
    public $js = [];
    public $moduloID = 0;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('tipos_usuarios_model');

        $this->moduloID                 = $this->admin_model->get_modulo_id('opciones');
        $this->data['moduloID']         = $this->moduloID;
        $this->data['tituloModulo']     = 'Tipos Usuarios';
        $this->data['columnas']         = ['id','descripcion','acciones'];
        $this->data['itemsMenu']        = array2Object([ 'parent'=>'tipos-usuarios', 'active'=>'' ]);
        $this->js['ajax_url']           = base_url('tiposUsuarios/listar');
        //$this->data['modal']            = $this->load->view('app/inc/modal',null,true);

        estaConectado();
        if(!puedeVer('opciones')){
            redirect(base_url('inicio'));
        }

    }

    public function index() {
        $this->data['js'] = $this->load->view('app/js/tipos_usuarios',$this->js,true);
        $this->load->view('app/inc/header',$this->data);
        $this->load->view('app/inc/nav');
        $this->load->view('app/tipos_usuarios/index');
        $this->load->view('app/inc/footer');
    }

    public function listar(){
        echo $this->tipos_usuarios_model->get_data();
    }

    public function nuevo(){
        if($this->input->post()){
            $campos = $this->input->post();
            $arr_excluidos = [];
            $formData = [];
            foreach($campos as $nombre => $valor):
                if(!in_array($nombre,$arr_excluidos)):
                    switch($nombre):
                        default: $formData[$nombre]=$valor; break;
                    endswitch;
                endif;
            endforeach;
            $result = $this->tipos_usuarios_model->add($formData);
            if($result['status'] == true):
                $this->session->set_flashdata('tipo','success');
                $this->session->set_flashdata('mensaje','Tipo de usuario registrado exitosamente!');
            else:
                $this->session->set_flashdata('tipo','danger');
                $this->session->set_flashdata('mensaje','Ocurrió un error al registrar los datos!');
            endif;
            redirect(base_url('tiposUsuarios'));
        }else{
            redirect(base_url('tiposUsuarios'));
        }
    }

    public function editar($id){
        $this->data['id']=$id;
        $this->data['datosItem'] = $this->tipos_usuarios_model->get($id);
        $this->data['js'] = $this->load->view('app/js/tipos_usuarios',$this->js,true);
        $this->load->view('app/inc/header',$this->data);
        $this->load->view('app/inc/nav');
        $this->load->view('app/tipos_usuarios/editar');
        $this->load->view('app/inc/footer');
    }

    public function editarItem($id){
        if($this->input->post()){
            $campos = $this->input->post();
            $arr_excluidos = [];
            $formData = [];
            foreach($campos as $nombre => $valor):
                if(!in_array($nombre,$arr_excluidos)):
                    switch($nombre):
                        default: $formData[$nombre]=$valor; break;
                    endswitch;
                endif;
            endforeach;
            $result = $this->tipos_usuarios_model->edit($id,$formData);
            if($result['status'] == true):
                $this->session->set_flashdata('tipo','success');
                $this->session->set_flashdata('mensaje','Tipo de usuario modificado exitosamente!');
            else:
                $this->session->set_flashdata('tipo','danger');
                $this->session->set_flashdata('mensaje','Ocurrió un error al registrar los datos!');
            endif;
            redirect(base_url("tiposUsuarios"));
        }else{
            redirect(base_url("tiposUsuarios"));
        }
    }

    public function activar($id){
        $this->tipos_usuarios_model->set_estado($id,'S');
        redirect($this->agent->referrer());
    }

    public function desactivar($id){
        $this->tipos_usuarios_model->set_estado($id,'N');
        redirect($this->agent->referrer());
    }

    public function eliminar($id){
        $result=$this->tipos_usuarios_model->delete($id);
        if($result['status'] == true):
            $this->session->set_flashdata('tipo','success');
            $this->session->set_flashdata('mensaje','Tipo de usuario eliminado existosamente!');
        else:
            $this->session->set_flashdata('tipo','danger');
            $this->session->set_flashdata('mensaje','Ocurrió un error al eliminar los datos!');
        endif;
        redirect($this->agent->referrer());
    }

}
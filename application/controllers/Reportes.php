<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reportes extends CI_Controller
{

    /*******************************************
     * COSAS POR HACER
     * Crear el controlador del módulo (grupo:lógica de negocio)
     * Crear las vistas del módulo (grupo: ui/ux)
     * Crear el modelo del módulo (grupo: modelo/base de datos)
     *******************************************/
    public $data = [];
    public $js = [];
    public $moduloID = 0;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('reportes_model');

        $this->moduloID                 = $this->admin_model->get_modulo_id('opciones');
        $this->data['moduloID']         = $this->moduloID;
        $this->data['tituloModulo']     = 'Reportes';
        $this->data['itemsMenu']        = array2Object([ 'parent'=>'reportes', 'active'=>'' ]);
        $this->js['ajax_url']           = base_url('tiposUsuarios/listar');
        //$this->data['modal']            = $this->load->view('app/inc/modal',null,true);

        estaConectado();
        if(!puedeVer('opciones')){
            redirect(base_url('inicio'));
        }

    }

    public function index() {
        $desde = "01/".date('m/Y');
        $hasta = date('t/m/Y');
        if($this->input->post()){
            $desde = $this->input->post('desde');
            $hasta = $this->input->post('hasta');
        }
        $this->data['desde'] = $desde;
        $this->data['hasta'] = $hasta;
        $this->data['pedidos'] = $this->reportes_model->get_pedidos_rango_fecha($desde,$hasta);
        $this->data['columnas'] = ['id','fecha registro','fecha pedido','usuario','detalle','total','estado'];
        $this->data['js'] = $this->load->view('app/js/reportes',$this->js,true);
        $this->load->view('app/inc/header',$this->data);
        $this->load->view('app/inc/nav');
        $this->load->view('app/reportes/index');
        $this->load->view('app/inc/footer');
    }

    public function listar(){
        echo $this->reportes_model->get_data();
    }

    public function nuevo(){
        if($this->input->post()){
            $campos = $this->input->post();
            $arr_excluidos = [];
            $formData = [];
            foreach($campos as $nombre => $valor):
                if(!in_array($nombre,$arr_excluidos)):
                    switch($nombre):
                        default: $formData[$nombre]=$valor; break;
                    endswitch;
                endif;
            endforeach;
            $result = $this->reportes_model->add($formData);
            if($result['status'] == true):
                $this->session->set_flashdata('tipo','success');
                $this->session->set_flashdata('mensaje','Tipo de usuario registrado exitosamente!');
            else:
                $this->session->set_flashdata('tipo','danger');
                $this->session->set_flashdata('mensaje','Ocurrió un error al registrar los datos!');
            endif;
            redirect(base_url('tiposUsuarios'));
        }else{
            redirect(base_url('tiposUsuarios'));
        }
    }

    public function editar($id){
        $this->data['id']=$id;
        $this->data['datosItem'] = $this->reportes_model->get($id);
        $this->data['js'] = $this->load->view('app/js/tipos_usuarios',$this->js,true);
        $this->load->view('app/inc/header',$this->data);
        $this->load->view('app/inc/nav');
        $this->load->view('app/tipos_usuarios/editar');
        $this->load->view('app/inc/footer');
    }

    public function editarItem($id){
        if($this->input->post()){
            $campos = $this->input->post();
            $arr_excluidos = [];
            $formData = [];
            foreach($campos as $nombre => $valor):
                if(!in_array($nombre,$arr_excluidos)):
                    switch($nombre):
                        default: $formData[$nombre]=$valor; break;
                    endswitch;
                endif;
            endforeach;
            $result = $this->reportes_model->edit($id,$formData);
            if($result['status'] == true):
                $this->session->set_flashdata('tipo','success');
                $this->session->set_flashdata('mensaje','Tipo de usuario modificado exitosamente!');
            else:
                $this->session->set_flashdata('tipo','danger');
                $this->session->set_flashdata('mensaje','Ocurrió un error al registrar los datos!');
            endif;
            redirect(base_url("tiposUsuarios"));
        }else{
            redirect(base_url("tiposUsuarios"));
        }
    }

    public function activar($id){
        $this->reportes_model->set_estado($id,'S');
        redirect($this->agent->referrer());
    }

    public function desactivar($id){
        $this->reportes_model->set_estado($id,'N');
        redirect($this->agent->referrer());
    }

    public function eliminar($id){
        $result=$this->reportes_model->delete($id);
        if($result['status'] == true):
            $this->session->set_flashdata('tipo','success');
            $this->session->set_flashdata('mensaje','Tipo de usuario eliminado existosamente!');
        else:
            $this->session->set_flashdata('tipo','danger');
            $this->session->set_flashdata('mensaje','Ocurrió un error al eliminar los datos!');
        endif;
        redirect($this->agent->referrer());
    }

    public function exportar($tipo,$desde,$hasta){
        $output = '';
        $ts = date('dmY-His');
        $filename = "reporte-comedor($desde-$hasta)_$ts.csv";
        $reporte = $this->reportes_model->reporte_pedidos_rango_fecha($desde,$hasta);
        $output .= implode(';',$reporte['columnas']).PHP_EOL;
        foreach($reporte['datos'] as $row):
            $arrData = [];
            foreach($row as $key => $value):
                $arrData[]=$value;
            endforeach;
            $output.=implode(';',$arrData).PHP_EOL;
        endforeach;
        //$output = mb_convert_encoding($output, 'UTF-16LE', 'UTF-8');
        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header("Content-Disposition: attachment; filename=$filename");
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        echo $output;
    }

}
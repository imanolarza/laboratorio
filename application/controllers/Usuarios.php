<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Usuarios extends CI_Controller {

    /*******************************************
     * COSAS POR HACER
     * Crear el controlador del módulo (grupo:lógica de negocio)
     * Crear las vistas del módulo (grupo: ui/ux)
     * Crear el modelo del módulo (grupo: modelo/base de datos)
     *******************************************/
    
    var $data = [];
    var $js = [];
    var $moduloID = 0;

	public function __construct(){

        parent::__construct();
        
        $this->load->model('usuarios_model');
        $this->load->model('modulos_model');
        $this->load->model('tipos_usuarios_model');

        $this->moduloID             = $this->admin_model->get_modulo_id('usuarios');
        $this->data['moduloID']     = $this->moduloID;
        $this->data['tituloModulo'] = 'Usuarios';
        $this->data['columnas']     = ['id','nombre','apellido','email','login','tipo','acciones'];
        $this->data['itemsMenu']    = array2Object([ 'parent'=>'usuarios', 'active'=>'' ]);
        $this->js['ajax_url']       = base_url('usuarios/listar');
        //$this->data['modal']        = $this->load->view('app/inc/modal',null,true);

        estaConectado();
        /* if(!puedeVer('usuarios')){
            redirect(base_url('inicio'));
        } */

    }

    public function index() {
        $this->data['tiposUsuarios'] = $this->usuarios_model->get_tipos_usuarios();
        $this->data['js'] = $this->load->view('app/js/usuarios',$this->js,true);
        $this->load->view('app/inc/header',$this->data);
        $this->load->view('app/inc/nav');
        $this->load->view('app/usuarios/index');
        $this->load->view('app/inc/footer');
    }

    public function listar(){
        echo $this->usuarios_model->get_data();
    }

    public function nuevo(){
        if($this->input->post()){
            $campos = $this->input->post();
            $arr_excluidos = [];
            $formData = [];
            foreach($campos as $nombre => $valor):
                if(!in_array($nombre,$arr_excluidos)):
                    switch($nombre):
                        case 'pass' : $formData[$nombre]=sha1($valor); break;
                        default     : $formData[$nombre]=$valor; break;
                    endswitch;
                endif;
            endforeach;
            $result = $this->usuarios_model->add($formData);
            if($result['status'] == true):
                $uid = $result['id'];
                // GENERAMOS LOS PERMISOS POR DEFAULT DEL USUARIO POR TIPO
                $modulos = $this->modulos_model->get_modulos();
                $tipo_usuario = $this->tipos_usuarios_model->get_tipo_usuario($formData['tipos_usuarios_id']);
                foreach($modulos as $modulo => $v):
                    switch(strtolower($tipo_usuario)){
                        case 'alumno'   : $dataPermisos = unserialize(PERMISOS_CLIENTE); break;
                        case 'profesor' : $dataPermisos = unserialize(PERMISOS_CLIENTE); break;
                        default         : $dataPermisos = unserialize(PERMISOS_CANTINERO); break;
                    }
                    $dataPermisos['modulos_id'] = $modulo;
                    $dataPermisos['usuarios_id'] = $uid;
                    $this->usuarios_model->set_permisos_modulo($dataPermisos);
                endforeach;
                $this->session->set_flashdata('tipo','success');
                $this->session->set_flashdata('mensaje','Usuario registrado exitosamente!');
            else:
                $this->session->set_flashdata('tipo','danger');
                $this->session->set_flashdata('mensaje','Ocurrió un error al registrar los datos!');
            endif;
            redirect(base_url('usuarios'));
        }else{
            redirect(base_url('usuarios'));
        }
    }

    public function nuevoUsuario(){
        if($this->input->post()){
            $campos = $this->input->post();
            $arr_excluidos = ['pass_confirm'];
            $formData = array('tipos_usuarios_id'=>1);
            foreach($campos as $nombre => $valor):
                if(!in_array($nombre,$arr_excluidos)):
                    switch($nombre):
                        case 'pass': $formData[$nombre]=sha1($valor); break;
                        default: $formData[$nombre]=$valor; break;
                    endswitch;
                endif;
            endforeach;
            $result = $this->usuarios_model->add($formData);
            if($result['status'] == true):
                $this->session->set_flashdata('tipo','success');
                $this->session->set_flashdata('mensaje','Usuario registrado exitosamente!');
            else:
                $this->session->set_flashdata('tipo','danger');
                $this->session->set_flashdata('mensaje','Ocurrió un error al registrar los datos!');
            endif;
            redirect(base_url('inicio/ingresar'));
        }else{
            redirect(base_url('inicio/ingresar'));
        }
    }

    public function activar($id){
        $this->usuarios_model->set_estado($id,'S');
        redirect($this->agent->referrer());
        //echo "se activo el $id";
    }

    public function desactivar($id){
        $this->usuarios_model->set_estado($id,'N');
        redirect($this->agent->referrer());
        //echo "se desactivo el $id";
    }

    public function eliminar($id){
        $result=$this->usuarios_model->delete($id);
        if($result['status'] == true):
            $this->session->set_flashdata('tipo','success');
            $this->session->set_flashdata('mensaje','Usuario eliminado existosamente!');
        else:
            $this->session->set_flashdata('tipo','danger');
            $this->session->set_flashdata('mensaje','Ocurrió un error al eliminar los datos!');
        endif;
        redirect($this->agent->referrer());
    }

    public function editar($id){
        $this->data['id']=$id;
        $this->data['modulos'] = $this->modulos_model->get_modulos();
        $this->data['permisos'] = $this->usuarios_model->get_permisos_usuario($id);
        $this->data['datosUsuario'] = $this->usuarios_model->get($id);
        $this->data['tiposUsuarios'] = $this->usuarios_model->get_tipos_usuarios();
        $this->data['js'] = $this->load->view('app/js/usuarios',$this->js,true);
        $this->load->view('app/inc/header',$this->data);
        $this->load->view('app/inc/nav');
        $this->load->view('app/usuarios/editar');
        $this->load->view('app/inc/footer');
    }

    public function editarUsuario($id){
        if($this->input->post()){
            $campos = $this->input->post();
            $arr_excluidos = ['permisos'];
            $formData = [];
            foreach($campos as $nombre => $valor):
                if(!in_array($nombre,$arr_excluidos)):
                    switch($nombre):
                        case 'pass': 
                            if(strlen($valor)>0):
                                $formData[$nombre]=sha1($valor);
                            endif;
                            break;
                        default: $formData[$nombre]=$valor; break;
                    endswitch;
                endif;
            endforeach;
            // var_dump($formData);
            // var_dump($this->input->post('permisos'));
            
            $result = $this->usuarios_model->edit($id,$formData);
            if($result['status'] == true):

                // procesamos los datos de permisos del usuario
                $this->usuarios_model->delete_permisos_usuario($id);
                foreach($this->input->post('permisos') as $modulo => $permisos):
                    $dataPermisos = [
                        'usuarios_id'=>$id,
                        'modulos_id'=>$modulo,
                        'leer'=>'N',
                        'crear'=>'N',
                        'editar'=>'N',
                        'eliminar'=>'N'
                    ];
                    foreach($dataPermisos as $tipo => $v):
                        if(isset($permisos[$tipo])):
                            $dataPermisos[$tipo]='S';
                        endif;
                    endforeach;
                    $this->usuarios_model->set_permisos_modulo($dataPermisos);
                endforeach;
                $this->session->set_flashdata('tipo','success');
                $this->session->set_flashdata('mensaje','Usuario modificado exitosamente!');
            else:
                $this->session->set_flashdata('tipo','danger');
                $this->session->set_flashdata('mensaje','Ocurrió un error al registrar los datos!');
            endif;
            redirect(base_url("usuarios"));
        }else{
            redirect(base_url("usuarios"));
        }
    }

    public function showPermisosTest(){
        $uid = $this->session->userdata('uid');
        $permisos = $this->usuarios_model->get_permisos($uid); 
        var_dump($permisos);
    }

    public function buscar(){
        $result = [];
        if($this->input->post('search')):
            $query = $this->input->post('search');
            $result = $this->usuarios_model->buscar_usuario($query);;
        endif;
        echo json_encode($result);
    }

}

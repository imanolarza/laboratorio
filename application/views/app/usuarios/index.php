<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $tituloModulo ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#"><?= $tituloModulo ?></a></li>
                        <li class="breadcrumb-item active">Listado de usuarios</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><?= $tituloModulo ?></h3>
                            <ul class="nav nav-pills ml-auto p-2">
                                <li class="nav-item"><a class="nav-link active" href="#tabla" data-toggle="tab">Listado</a></li>
                                <?php if(puedeCrear($moduloID)): ?>
                                <li class="nav-item"><a class="nav-link" href="#nuevo" data-toggle="tab">Nuevo</a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                            <?php if($this->session->flashdata('mensaje')): $tipo = $this->session->flashdata('tipo'); ?>
                                <div class="alert alert-<?=$tipo?> alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h5><i class="icon fas fa-exclamation-triangle"></i> Atención!</h5>
                                <?=$this->session->flashdata('mensaje')?>
                                </div>
                            <?php endif; ?>
                                <div class="tab-pane active" id="tabla">
                                    <!-- tabla-usuarios -->
                                    <table id="tblUsuarios" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="tblUsuarios_info">
                                        <thead>
                                        <tr>
                                            <? foreach($columnas as $col){ ?>
                                                <th><?=ucfirst($col)?></th>
                                            <? } ?>
                                        </tr>
                                        </thead>
                                    </table>
                                    <!-- /tabla-usuarios -->
                                </div>
                                <?php if(puedeCrear($moduloID)): ?>
                                <div class="tab-pane" id="nuevo">
                                    <!-- formulario-nuevo -->
                                    <div class="card card-primary">
                                        <div class="card-header">
                                            <h3 class="card-title">Carga de nuevo usuario</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <!-- form start -->
                                        <form action="<?=base_url('usuarios/nuevo')?>" method="post" enctype="multipart/form-data" role="form">
                                            <div class="card-body">
                                                <!-- nombre -->
                                                <div class="form-group">
                                                    <label for="nombre">Nombre</label>
                                                    <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre del usuario">
                                                </div>
                                                <!-- apellido -->
                                                <div class="form-group">
                                                    <label for="apellido">Apellido</label>
                                                    <input type="text" class="form-control" name="apellido" id="apellido" placeholder="Apellido del usuario">
                                                </div>
                                                <!-- telefono -->
                                                <div class="form-group">
                                                    <label for="telefono">Nro. de teléfono</label>
                                                    <input type="text" class="form-control" name="telefono" id="telefono" placeholder="Teléfono del usuario">
                                                </div>
                                                <!-- cedula -->
                                                <div class="form-group">
                                                    <label for="cedula">Nro. de cédula</label>
                                                    <input type="text" class="form-control" name="cedula" id="cedula" placeholder="Nro de documento">
                                                </div>
                                                <!-- email,tipo_usuario -->
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="email">Email</label>
                                                            <input type="email" class="form-control" name="email" id="email" placeholder="Dirección de email">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Tipo de usuario</label>
                                                            <select name="tipos_usuarios_id" id="tipos_usuarios_id" class="form-control select2" style="width: 100%;">
                                                                <?php foreach($tiposUsuarios as $id => $tipo): ?>
                                                                <option value="<?=$id?>"><?=$tipo?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="login">Login</label>
                                                            <input type="text" class="form-control" name="login" id="login" placeholder="Nombre de usuario">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="pass">Password</label>
                                                            <input type="password" class="form-control" name="pass" id="pass" placeholder="*********">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!-- /.card-body -->

                                            <div class="card-footer">
                                            <button type="submit" class="btn btn-primary">Guardar</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /formulario-nuevo -->
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

</div>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $tituloModulo ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#"><?= $tituloModulo ?></a></li>
                        <li class="breadcrumb-item active">Editar</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                <form action="<?=base_url("usuarios/editarUsuario/$id")?>" method="post" enctype="multipart/form-data" role="form">
                    <div class="card">
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3">Editar Usuario</h3>
                            <ul class="nav nav-pills ml-auto p-2">
                                <li class="nav-item"><a class="nav-link active" href="#datos" data-toggle="tab">Datos</a></li>
                                <li class="nav-item"><a class="nav-link" href="#permisos" data-toggle="tab">Permisos</a></li>
                            </ul>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="datos">
                                    <!-- nombre -->
                                    <div class="form-group">
                                        <label for="nombre">Nombre</label>
                                        <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre del usuario" value="<?=$datosUsuario->nombre?>">
                                    </div>
                                    <!-- apellido -->
                                    <div class="form-group">
                                        <label for="apellido">Apellido</label>
                                        <input type="text" class="form-control" name="apellido" id="apellido" placeholder="Apellido del usuario" value="<?=$datosUsuario->apellido?>">
                                    </div>
                                    <!-- telefono -->
                                    <div class="form-group">
                                        <label for="telefono">Nro. de teléfono</label>
                                        <input type="text" class="form-control" name="telefono" id="telefono" placeholder="Teléfono del usuario" value="<?=$datosUsuario->telefono?>">
                                    </div>
                                    <!-- cedula -->
                                    <div class="form-group">
                                        <label for="cedula">Nro. de cédula</label>
                                        <input type="text" class="form-control" name="cedula" id="cedula" placeholder="Nro de documento" value="<?=$datosUsuario->cedula?>">
                                    </div>
                                    <!-- email,tipo_usuario -->
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="email" class="form-control" name="email" id="email" placeholder="Dirección de email" value="<?=$datosUsuario->email?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Tipo de usuario</label>
                                                <select name="tipos_usuarios_id" id="tipos_usuarios_id" class="form-control select2" style="width: 100%;">
                                                <?php $valor =  $datosUsuario->tipos_usuarios_id; ?>    
                                                <?php foreach($tiposUsuarios as $id => $tipo): $selected = ($valor == $id)?'selected="selected"':'';  ?>
                                                    <option value="<?=$id?>" <?=$selected?> ><?=$tipo?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="login">Login</label>
                                                <input type="text" class="form-control" name="login" id="login" placeholder="Nombre de usuario" value="<?=$datosUsuario->login?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="pass">Password</label>
                                                <input type="password" class="form-control" name="pass" id="pass" placeholder="*********">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab-pane" id="permisos">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Módulo</th>
                                                <th>Leer</th>
                                                <th>Crear</th>
                                                <th>Editar</th>
                                                <th>Eliminar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <tbody>
                                            <?php foreach($modulos as $id => $modulo): ?>
                                            <tr>
                                                <td><?=$id?></td>
                                                <td><?=$modulo?></td>
                                                <td>
                                                    <div class="form-check">
                                                    <?php 
                                                            $checked = '';
                                                            if(isset($permisos[$id])):
                                                                $checked = ($permisos[$id]->leer == 'S')?'checked':''; 
                                                            endif;
                                                        ?>
                                                        <input class="form-check-input" name="permisos[<?=$id?>][leer]" type="checkbox" <?=$checked?> >
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <?php 
                                                            $checked = '';
                                                            if(isset($permisos[$id])):
                                                                $checked = ($permisos[$id]->crear == 'S')?'checked':''; 
                                                            endif;
                                                        ?>
                                                        <input class="form-check-input" name="permisos[<?=$id?>][crear]" type="checkbox" <?=$checked?> >
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <?php
                                                            $checked = '';
                                                            if(isset($permisos[$id])):
                                                                $checked = ($permisos[$id]->editar == 'S')?'checked':''; 
                                                            endif;
                                                        ?>
                                                        <input class="form-check-input" name="permisos[<?=$id?>][editar]" type="checkbox" <?=$checked?> >
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                    <?php 
                                                            $checked = '';
                                                            if(isset($permisos[$id])):
                                                                $checked = ($permisos[$id]->eliminar == 'S')?'checked':''; 
                                                            endif;
                                                        ?>
                                                        <input class="form-check-input" name="permisos[<?=$id?>][eliminar]" type="checkbox" <?=$checked?> >
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </section>
</div>
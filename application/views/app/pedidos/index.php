<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $tituloModulo ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#"><?= $tituloModulo ?></a></li>
                        <li class="breadcrumb-item active">Listado de Pedidos</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><?= $tituloModulo ?></h3>
                            <?php if($esCantinero): ?>
                            <div>
                                <span class="btn btn-info mt-2 mr-1">Total de pedidos: <?=$pedidosTotal?></span>
                                <span class="btn btn-warning mt-2 mr-1">Pendientes: <?=$pedidosPendientes?></span>
                                <span class="btn btn-danger mt-2 mr-1">Cancelados: <?=$pedidosCancelados?></span>
                            </div>
                            <?php endif; ?>
                            <ul class="nav nav-pills ml-auto p-2">
                                <li class="nav-item"><a class="nav-link active" href="#tabla" data-toggle="tab">Listado</a></li>
                                <li class="nav-item"><a class="btn btn-success ml-1" href="<?=base_url('pedidos/cargar')?>">Cargar pedido</a></li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                            <?php if($this->session->flashdata('mensaje')): $tipo = $this->session->flashdata('tipo'); ?>
                                <div class="alert alert-<?=$tipo?> alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h5><i class="icon fas fa-exclamation-triangle"></i> Atención!</h5>
                                <?=$this->session->flashdata('mensaje')?>
                                </div>
                            <?php endif; ?>
                                <div class="tab-pane active" id="tabla">
                                    <!-- tabla-pedidos -->
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-1 mt-2 text-right" for="desde">Desde</label>
                                            <div class="col-2">
                                                <div class="input-group date" id="control-desde" data-target-input="nearest">
                                                    <input type="text" name="desde" id="desde" class="form-control datetimepicker-input" data-target="#control-desde" value="<?= $desde ?>" />
                                                    <div class="input-group-append" data-target="#control-desde" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>    
                                            <label class="col-1 mt-2 text-right" for="hasta">Hasta</label>
                                            <div class="col-2">
                                                <div class="input-group date" id="control-hasta" data-target-input="nearest">
                                                    <input type="text" name="hasta" id="hasta" class="form-control datetimepicker-input" data-target="#control-hasta" value="<?= $hasta ?>" />
                                                    <div class="input-group-append" data-target="#control-hasta" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <button type="button" id="btnFiltrar" data-url="<?=base_url('pedidos/index')?>" class="btn btn-dark">Filtrar</button>
                                            </div>   
                                        </div>
                                    </div>
                                    <table id="tblPedidos" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="tblUsuarios_info">
                                        <thead>
                                        <tr>
                                            <? foreach($columnas as $col){ ?>
                                                <th><?=ucfirst($col)?></th>
                                            <? } ?>
                                        </tr>
                                        </thead>
                                    </table>
                                    <!-- /tabla-pedidos -->
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

</div>
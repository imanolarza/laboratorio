<?php $detallesPedido = ($this->session->userdata('detallesPedido')) ? $this->session->userdata('detallesPedido') : []; ?>
<?php $productosPedido = (count($detallesPedido) > 0) ? array_keys($detallesPedido) : []; ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $tituloModulo ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#"><?= $tituloModulo ?></a></li>
                        <li class="breadcrumb-item active">Detalles del pedido</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <!-- formulario-editar -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Detalles del pedido</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        
                            <div class="card-body">

                                <?php if ($this->session->flashdata('mensaje')) : $tipo = $this->session->flashdata('tipo'); ?>
                                    <div class="alert alert-<?= $tipo ?> alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h5><i class="icon fas fa-exclamation-triangle"></i> Atención!</h5>
                                        <?= $this->session->flashdata('mensaje') ?>
                                    </div>
                                <?php endif; ?>

                                <!-- fecha, menu -->
                                <div class="form-group">
                                    <div class="row">
                                        <label for="descripcion" class="col-xs-12 col-sm-2">Fecha del pedido</label>
                                        <div class="col-xs-12 col-sm-2">
                                            <div class="input-group date" id="control-fecha" data-target-input="nearest">
                                                <?php 
                                                $dataFecha = explode('-',$datosPedido->fecha_pedido);
                                                $dataFecha = array_reverse($dataFecha);
                                                $fechaPedido = implode('/',$dataFecha);
                                                ?>
                                                <input type="text" name="fecha" id="fecha" class="form-control datetimepicker-input" data-target="#control-fecha" value="<?= $fechaPedido ?>" />
                                                <div class="input-group-append" data-target="#control-fecha" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-8"></div>
                                    </div>
                                </div>
                                <!-- observacion -->
                                <div class="form-group">
                                    <div class="row">
                                        <label for="observacion" class="col-xs-12 col-sm-2">Observacion</label>
                                        <div class="col-xs-12 col-sm-10">
                                            <textarea name="observacion" id="observacion" class="form-control" rows="5" placeholder="Ingresar observación ...">
                                            <?=$datosPedido->observacion?>
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                <!-- productos -->
                                <div class="form-group productos">
                                    <?php foreach ($datosProductos as $p) : ?>
                                        <label>
                                            <div class="card mr-1" style="width: 12rem;">
                                                <?php $checked = (in_array($p->id, $productosPedido)) ? 'checked' : ''; ?>
                                                <?php
                                                $img = "default.png";
                                                if (strlen($p->imagen_1) > 0) {
                                                    $img = $p->imagen_1;
                                                } else if (strlen($p->imagen_2) > 0) {
                                                    $img = $p->imagen_2;
                                                }
                                                ?>
                                                <img src="<?= base_url("assets/media/productos/$img") ?>" class="card-img-top" alt="Imagen de producto" />
                                                <div class="card-body">
                                                    <h5 class="card-title"><?= $p->descripcion ?></h5>
                                                    <p class="card-text"><?= $tiposProductos[$p->tipo] ?></p>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <p>Gs. <?= number_format($p->precio, 0, ',', '.') ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </label>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-4"></div>
                                    <div class="col-8 text-right">
                                        TOTAL DEL PEDIDO: Gs. <span class="btn btn-danger"><?= number_format($datosPedido->monto, 0, ',', '.') ?></span>
                                        <button type="button" class="btn btn-info" id="btnCarrito">
                                            <span class="fa fa-shopping-cart"></span>
                                            <small class="badge badge-dark"><?= count($datosProductos) ?></small>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        
                    </div>
                    <!-- /formulario-editar -->

                </div>
            </div>
        </div>
    </section>
</div>
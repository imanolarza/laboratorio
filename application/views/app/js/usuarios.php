<script language="javascript">
    $(function() {

        $("#tblUsuarios").DataTable({
            "responsive": true,
            "autoWidth": false,
            "bProcessing": true,
            "sAjaxSource": "<?=$ajax_url?>",
            "language": {
                "url": "<?=base_url()?>tpl/plugins/datatables/jquery.dataTables.lang.es.json"
            },
            "aoColumns": [
                { "sWidth": "30px" },
                { "sWidth": "auto" },
                { "sWidth": "auto" },
                { "sWidth": "auto" },
                { "sWidth": "auto" },
                { "sWidth": "auto" },
                { "sWidth": "140px" }
            ]
        });

        //Initialize Select2 Elements
        $('.select2').select2();

    });
</script>
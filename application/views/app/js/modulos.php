<script language="javascript">
    $(function() {

        $("#tblUsuarios").DataTable({
            "responsive": true,
            "autoWidth": false,
            "bProcessing": true,
            "sAjaxSource": "<?=$ajax_url?>",
            "language": {
                "url": "<?=base_url()?>tpl/plugins/datatables/jquery.dataTables.lang.es.json"
            },
            "aoColumns": [
                { "sWidth": "30px" },
                { "sWidth": "auto" },
                { "sWidth": "120px" }
            ]
        });

        <?php if($this->session->flashdata('tipo')): ?>
        $('#modal-alerta .modal-header h4').text('Atención!');
        $('#modal-alerta .modal-body').html('<p><?=$this->session->flashdata('mensaje')?></p>');
        $('#modal-alerta').modal('show');
        <?php endif; ?>

    });
</script>
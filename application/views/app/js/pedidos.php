<script language="javascript">
    $(function() {

        $("#tblPedidos").DataTable({
            "responsive": true,
            "autoWidth": false,
            "bProcessing": true,
            "sAjaxSource": "<?=$ajax_url?>",
            "language": { "url": "<?=base_url("tpl/plugins/datatables/jquery.dataTables.lang.es.json")?>" },
            "aoColumns": [
                { "sWidth": "30px" },
                { "sWidth": "auto" },
                { "sWidth": "auto" },
                { "sWidth": "auto" },
                { "sWidth": "auto" },
                { "sWidth": "auto" },
                { "sWidth": "180px" }
            ],
            "order":[[0,"desc"]]
        });
        $('.select2').select2({'width':'100%'});
        $('#control-fecha').datetimepicker({ format: 'DD/MM/YYYY' });
        $('#control-desde').datetimepicker({ format: 'DD/MM/YYYY' });
        $('#control-hasta').datetimepicker({ format: 'DD/MM/YYYY' });
        $('#menu').on('select2:select', function (e) {
            var data = e.params.data;
            var urlRedireccion = '<?=base_url('pedidos/cargar/')?>'+data.id;
            window.location.replace(urlRedireccion);
        });

        <?php if($tipoUsuario == 'CANTINA' || $tipoUsuario == 'ADMINISTRADOR'): ?>
        $('#btnConfirmar').on('click',function(e){
            var fechaPedido = $('#fecha').val();
            var uid = $('#buscar').val();
            console.log(fechaPedido);
            console.log(uid);
            if(fechaPedido.length > 0 && uid != null){
                var url = $(this).data('url');
                var arrFechaPedido = fechaPedido.split('/');
                fechaPedido = arrFechaPedido[2]+"-"+arrFechaPedido[1]+"-"+arrFechaPedido[0]
                window.location.replace(url+"/"+fechaPedido+"/"+uid);
            }else{
                if(fechaPedido.length == 0){
                    alert('Cargar la fecha del pedido');
                }else{
                    alert('Seleccione el usuario para el pedido');
                }
            }
        });
        <?php else: ?>
            $('#btnConfirmar').on('click',function(e){
                var fechaPedido = $('#fecha').val();
                if(fechaPedido.length > 0){
                    var url = $(this).data('url');
                    var arrFechaPedido = fechaPedido.split('/');
                    fechaPedido = arrFechaPedido[2]+"-"+arrFechaPedido[1]+"-"+arrFechaPedido[0]
                    window.location.replace(url+"/"+fechaPedido);
                }else{
                    alert('Cargar la fecha del pedido');
                }
            });
    <?php endif; ?>

        $('#buscar').select2({
            'width':'100%',
            ajax: {
                url: '<?=base_url('usuarios/buscar')?>',
                dataType: 'json',
                method: 'post',
                delay: 250,
                data: function (params) {
                    var queryParameters = {
                        search: params.term
                    }
                    return queryParameters;
                },
                processResults: function (data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    return { results: data.items };
                }
            }
        });

        $('#btnFiltrar').on('click',function(e){
            
            var fDesde = ($('#desde').val() == undefined)?'':$('#desde').val();
            var fHasta =  ($('#hasta').val() == undefined)?'':$('#hasta').val();
            
            console.log(fDesde);
            console.log(fHasta);
            if(fDesde.length > 0 && fHasta.length > 0){
                var url = $(this).data('url');
                var arrFecha = fDesde.split('/');
                fDesde = arrFecha[0]+"-"+arrFecha[1]+"-"+arrFecha[2];
                arrFecha = fHasta.split('/');
                fHasta = arrFecha[0]+"-"+arrFecha[1]+"-"+arrFecha[2];
                console.log(fDesde);
                console.log(fHasta);
                window.location.replace(url+"/"+fDesde+"/"+fHasta);
            }else{
                alert('Favor ingresar el rango de fechas desde-hasta');
            }
        });


    });
</script>
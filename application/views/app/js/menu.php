<script language="javascript">
    $(function() {

        $("#tblUsuarios").DataTable({
            "responsive": true,
            "autoWidth": false,
            "bProcessing": true,
            "sAjaxSource": "<?=$ajax_url?>",
            "language": { "url": "<?=base_url()?>tpl/plugins/datatables/jquery.dataTables.lang.es.json" },
            "aoColumns": [
                { "sWidth": "auto" },
                { "sWidth": "auto" },
                { "sWidth": "auto" },
                { "sWidth": "120px" }
            ]
        });

        $("input[data-bootstrap-switch]").each(function(){
            $(this).bootstrapSwitch('state', $(this).prop('checked'));
        });

    });
</script>
<script language="javascript">
    $(function() {

        $("#tblData").DataTable({
            "responsive": true,
            "autoWidth": false,
            "language": {
                "url": "<?=base_url()?>tpl/plugins/datatables/jquery.dataTables.lang.es.json"
            },
            "aoColumns": [
                { "sWidth": "30px" },
                { "sWidth": "80px" },
                { "sWidth": "80px" },
                { "sWidth": "180px" },
                { "sWidth": "auto" },
                { "sWidth": "100px" },
                { "sWidth": "100px" }
            ]
        });

        $('#control-desde').datetimepicker({ format: 'DD/MM/YYYY' });
        $('#control-hasta').datetimepicker({ format: 'DD/MM/YYYY' });

        $('#btnExportar').on('click',function(e){
            var fechaDesde = $('#desde').val();
            var fechaHasta = $('#hasta').val();
            console.log(fechaDesde);
            console.log(fechaHasta);
            if(fechaHasta.length > 0 && fechaDesde.length > 0){
                var url = $(this).data('url');
                var arrFecha = fechaDesde.split('/');
                fechaDesde = arrFecha[0]+"-"+arrFecha[1]+"-"+arrFecha[2];
                arrFecha = fechaHasta.split('/');
                fechaHasta = arrFecha[0]+"-"+arrFecha[1]+"-"+arrFecha[2];
                window.location.replace(url+"/"+fechaDesde+"/"+fechaHasta);
            }else{
                if(fechaDesde.length == 0){
                    alert('Cargar la fecha inicial del rango');
                }else if(fechaHasta.length == 0){
                    alert('Cargar la fecha final del rango');
                }
            }
        });

    });

</script>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <strong>Copyright &copy; 2020 3BTI CTJ</strong>
    Todos los derechos reservados.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0.0
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?=base_url()?>tpl/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?=base_url()?>tpl/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?=base_url()?>tpl/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- PopperJS -->
<script src="<?=base_url()?>assets/popper/popper.min.js"></script>
<!-- ChartJS -->
<script src="<?=base_url()?>tpl/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?=base_url()?>tpl/plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="<?=base_url()?>tpl/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?=base_url()?>tpl/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?=base_url()?>tpl/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?=base_url()?>tpl/plugins/moment/moment.min.js"></script>
<script src="<?=base_url()?>tpl/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?=base_url()?>tpl/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?=base_url()?>tpl/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?=base_url()?>tpl/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- DataTables -->
<script src="<?=base_url()?>tpl/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>tpl/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>tpl/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=base_url()?>tpl/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?=base_url()?>tpl/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="<?=base_url()?>tpl/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- Select2 -->
<script src="<?=base_url()?>tpl/plugins/select2/js/select2.full.min.js"></script>
<!-- SweetAlert2 -->
<script src="<?=base_url()?>tpl/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>tpl/dist/js/adminlte.js"></script>
<script src="<?=base_url()?>assets/js/main.js"></script>
<script> $(function () { $('[data-toggle="tooltip"]').tooltip() }); </script>

<?php echo (isset($js)?$js:''); ?>

<?php echo (isset($modal)?$modal:''); ?>

</body>
</html>
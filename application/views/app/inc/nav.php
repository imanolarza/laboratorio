<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
  
<!-- Left navbar links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
    </li>
  </ul>

  <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto">
    <!-- Notifications Dropdown Menu -->
    <li class="nav-item dropdown">
      <a class="nav-link" data-toggle="dropdown" href="#">
        <i class="far fa-user-circle"></i>
        <!-- <span class="badge badge-warning navbar-badge">15</span> -->
      </a>
      <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
        <a href="<?=base_url('usuarios/editar/'.getUID())?>" class="dropdown-item">
          <i class="fas fa-id-card mr-2"></i> Mis datos
        </a>
        <div class="dropdown-divider"></div>
        <a href="<?=base_url('inicio/salir')?>" class="dropdown-item dropdown-footer text-left">
          <i class="nav-icon far fa-times-circle mr-2"></i> Cerrar sesión
        </a>
      </div>
    </li>
  </ul>

</nav>
<!-- /.navbar -->

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="#" class="brand-link text-center">
    <span class="brand-text"><?=NOMBRE_PROYECTO?></span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">

    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="info">
        <a href="#" class="d-block"><?=getSaludoUsuario(SALUDO_USUARIO)?></a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        
        <!-- dashboard -->
        <li class="nav-item has-treeview <?=($itemsMenu->parent === 'inicio')?'menu-open':''?>"> 
          <a href="#" class="nav-link <?=($itemsMenu->parent === 'inicio')?'active':''?>"> <!-- aqui agregar class active -->
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>Inicio<i class="right fas fa-angle-left"></i></p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="#" class="nav-link <?=($itemsMenu->active === 'inicio')?'active':''?>"> <!-- aqui agregar class active -->
                <i class="far fa-circle nav-icon "></i>
                <p>Inicio</p>
              </a>
            </li>
          </ul>
        </li>
        <!-- usuarios -->
        <li class="nav-item has-treeview <?=($itemsMenu->parent === 'usuarios')?'menu-open':''?>">
          <a href="#" class="nav-link <?=($itemsMenu->parent === 'usuarios')?'active':''?>"> <!-- aqui agregar class active -->
            <i class="nav-icon fas fa-copy"></i>
            <p>Usuarios<i class="fas fa-angle-left right"></i></p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?=base_url('usuarios')?>" class="nav-link"> <!-- aqui agregar class active -->
                <i class="far fa-circle nav-icon"></i>
                <p>Lista usuarios</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Clientes</p>
              </a>
            </li>
          </ul>
        </li>
        <!-- pedidos -->
        <li class="nav-item has-treeview <?=($itemsMenu->parent === 'pedidos')?'menu-open':''?>">
          <a href="<?=base_url('pedidos')?>" class="nav-link <?=($itemsMenu->parent === 'pedidos')?'active':''?>"> <!-- aqui agregar class active -->
            <i class="nav-icon fas fa-copy"></i>
            <p>Pedidos<i class="fas fa-angle-left right"></i></p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?=base_url('pedidos')?>" class="nav-link"><!-- aqui agregar class active -->
                <i class="far fa-circle nav-icon"></i>
                <p>Lista Pedidos</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?=base_url('pedidos/nuevo')?>" class="nav-link"><!-- aqui agregar class active -->
                <i class="far fa-circle nav-icon"></i>
                <p>Menú del día</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview <?=($itemsMenu->parent === 'productos')?'menu-open':''?>">
          <a href="#" class="nav-link <?=($itemsMenu->parent === 'productos')?'active':''?>"> <!-- aqui agregar class active -->
            <i class="nav-icon fas fa-copy"></i>
            <p>Productos<i class="fas fa-angle-left right"></i></p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?=base_url('productos')?>" class="nav-link"><!-- aqui agregar class active -->
                <i class="far fa-circle nav-icon"></i>
                <p>Lista Productos</p>
              </a>
            </li>
          </ul>
        </li>
        <!-- menu -->
        <li class="nav-item has-treeview <?=($itemsMenu->parent === 'menu')?'menu-open':''?>">
          <a href="#" class="nav-link <?=($itemsMenu->parent === 'menu')?'active':''?>"> <!-- aqui agregar class active -->
            <i class="nav-icon fas fa-copy"></i>
            <p>Menús<i class="fas fa-angle-left right"></i></p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?=base_url('menu')?>" class="nav-link"><!-- aqui agregar class active -->
                <i class="far fa-circle nav-icon"></i>
                <p>Lista de items</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?=base_url('menu/menuSemana')?>" class="nav-link"><!-- aqui agregar class active -->
                <i class="far fa-circle nav-icon"></i>
                <p>Menús de la semana</p>
              </a>
            </li>
          </ul>
        </li>
        <!-- reportes -->
        <li class="nav-item has-treeview <?=($itemsMenu->parent === 'reportes')?'menu-open':''?>">
          <a href="#" class="nav-link <?=($itemsMenu->parent === 'reportes')?'active':''?>"> <!-- aqui agregar class active -->
            <i class="nav-icon fas fa-copy"></i>
            <p>Reportes<i class="fas fa-angle-left right"></i></p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?=base_url('reportes')?>" class="nav-link"><!-- aqui agregar class active -->
                <i class="far fa-circle nav-icon"></i>
                <p>Pedidos x Rango de Fecha</p>
              </a>
            </li>
          </ul>
        </li>
        
        <!-- opciones -->
        <li class="nav-header">OPCIONES</li>
        <li class="nav-item">
          <a href="<?=base_url('tiposUsuarios')?>" class="nav-link <?=($itemsMenu->parent === 'tipos-usuarios')?'active':''?>"> <!-- aqui agregar class active -->
            <i class="nav-icon fa fa-cog"></i>
            <p>Tipos de usuario</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="<?=base_url('modulos')?>" class="nav-link <?=($itemsMenu->parent === 'modulos')?'active':''?>"> <!-- aqui agregar class active -->
            <i class="nav-icon fa fa-cog"></i>
            <p>Módulos</p>
          </a>
        </li>
        
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
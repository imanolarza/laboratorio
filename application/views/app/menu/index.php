<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $tituloModulo ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#"><?= $tituloModulo ?></a></li>
                        <li class="breadcrumb-item active">Listado de Menus</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><?= $tituloModulo ?></h3>
                            <ul class="nav nav-pills ml-auto p-2">
                                <li class="nav-item"><a class="nav-link active" href="#tabla" data-toggle="tab">Listado</a></li>
                                <li class="nav-item"><a class="nav-link" href="#nuevo" data-toggle="tab">Nuevo</a></li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                            <?php if($this->session->flashdata('mensaje')): $tipo = $this->session->flashdata('tipo'); ?>
                                <div class="alert alert-<?=$tipo?> alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h5><i class="icon fas fa-exclamation-triangle"></i> Atención!</h5>
                                <?=$this->session->flashdata('mensaje')?>
                                </div>
                            <?php endif; ?>
                                <div class="tab-pane active" id="tabla">
                                    <!-- tabla-usuarios -->
                                    <table id="tblUsuarios" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="tblUsuarios_info">
                                        <thead>
                                        <tr>
                                            <? foreach($columnas as $col){ ?>
                                                <th><?=ucfirst($col)?></th>
                                            <? } ?>
                                        </tr>
                                        </thead>
                                    </table>
                                    <!-- /tabla-usuarios -->
                                </div>
                                <div class="tab-pane" id="nuevo">
                                    <!-- formulario-nuevo -->
                                    <div class="card card-primary">
                                        <div class="card-header">
                                            <h3 class="card-title">Carga de nuevo menú</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <!-- form start -->
                                        <form action="<?=base_url('menu/nuevo')?>" method="post" enctype="multipart/form-data" role="form">
                                            <div class="card-body">
                                                <!-- descripcion -->
                                                <div class="form-group">
                                                    <label for="descripcion">Descripción</label>
                                                    <input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripción">
                                                </div>
                                                <!-- dias -->
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-12"><label>El menú estará disponible los días:</label></div>
                                                        <div class="col-xs-12 col-sm-2">
                                                            <input type="checkbox" name="lu" id="lu" checked data-bootstrap-switch value="S">
                                                            <label for="lu" class="ml-1">Lunes</label>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-2">
                                                            <input type="checkbox" name="ma" id="ma" checked data-bootstrap-switch value="S">
                                                            <label for="ma" class="ml-1">Martes</label>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-2">
                                                            <input type="checkbox" name="mi" id="mi" checked data-bootstrap-switch value="S">
                                                            <label for="mi" class="ml-1">Miercoles</label>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-2">
                                                            <input type="checkbox" name="ju" id="ju" checked data-bootstrap-switch value="S">
                                                            <label for="ju" class="ml-1"> Jueves</label>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-2">
                                                            <input type="checkbox" name="vi" id="vi" checked data-bootstrap-switch value="S">
                                                            <label for="vi" class="ml-1"> Viernes</label>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-2">&nbsp;</div>
                                                    </div>
                                                </div>
                                                <!-- productos -->
                                                <div class="form-group productos">
                                                    <?php foreach($productos as $p): ?>
                                                        <label>
                                                            <div class="card mr-1" style="width: 12rem;">
                                                                <?php 
                                                                $img = "default.png";
                                                                if( strlen($p->imagen_1) > 0 ){
                                                                    $img = $p->imagen_1;
                                                                }else if(strlen($p->imagen_2) > 0){
                                                                    $img = $p->imagen_2;
                                                                }
                                                                ?>
                                                                <img src="<?=base_url("assets/media/productos/$img")?>" class="card-img-top" alt="Imagen de producto" />
                                                                <div class="card-body">
                                                                    <h5 class="card-title"><?=$p->descripcion?></h5>
                                                                    <p class="card-text"><?=$tiposProductos[$p->tipo]?></p>
                                                                    <input type="checkbox" name="productos[<?=$p->id?>]" id="productos_<?=$p->id?>" >
                                                                </div>
                                                            </div>
                                                        </label>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                            <!-- /.card-body -->

                                            <div class="card-footer">
                                            <button type="submit" class="btn btn-primary">Guardar</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /formulario-nuevo -->
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

</div>
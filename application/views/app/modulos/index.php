<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $tituloModulo ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#"><?= $tituloModulo ?></a></li>
                        <li class="breadcrumb-item active">Listado de Módulos</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><?= $tituloModulo ?></h3>
                            <ul class="nav nav-pills ml-auto p-2">
                                <li class="nav-item"><a class="nav-link active" href="#tabla" data-toggle="tab">Listado</a></li>
                                <li class="nav-item"><a class="nav-link" href="#nuevo" data-toggle="tab">Nuevo</a></li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                            <?php if($this->session->flashdata('mensaje')): $tipo = $this->session->flashdata('tipo'); ?>
                                <div class="alert alert-<?=$tipo?> alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h5><i class="icon fas fa-exclamation-triangle"></i> Atención!</h5>
                                <?=$this->session->flashdata('mensaje')?>
                                </div>
                            <?php endif; ?>
                                <div class="tab-pane active" id="tabla">
                                    <!-- tabla-usuarios -->
                                    <table id="tblUsuarios" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="tblUsuarios_info">
                                        <thead>
                                        <tr>
                                            <? foreach($columnas as $col){ ?>
                                                <th><?=ucfirst($col)?></th>
                                            <? } ?>
                                        </tr>
                                        </thead>
                                    </table>
                                    <!-- /tabla-usuarios -->
                                </div>
                                <div class="tab-pane" id="nuevo">
                                    <!-- formulario-nuevo -->
                                    <div class="card card-primary">
                                        <div class="card-header">
                                            <h3 class="card-title">Carga de nuevo módulo</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <!-- form start -->
                                        <form action="<?=base_url('modulos/nuevo')?>" method="post" enctype="multipart/form-data" role="form">
                                            <div class="card-body">
                                                <!-- descripcion -->
                                                <div class="form-group">
                                                    <label for="descripcion">Descripción</label>
                                                    <input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripción">
                                                </div>
                                            </div>
                                            <!-- /.card-body -->

                                            <div class="card-footer">
                                            <button type="submit" class="btn btn-primary">Guardar</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /formulario-nuevo -->
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

</div>
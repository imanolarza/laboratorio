<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $tituloModulo ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#"><?= $tituloModulo ?></a></li>
                        <li class="breadcrumb-item active">Editar</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <!-- formulario-editar -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Editar módulo</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="<?=base_url("modulos/editarItem/$id")?>" method="post" enctype="multipart/form-data" role="form">
                            <div class="card-body">
                                <!-- descripcion -->
                                <div class="form-group">
                                    <label for="descripcion">Descripción</label>
                                    <input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripción de tipo de usuario" value="<?=$datosItem->descripcion?>">
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <?php if(puedeEditar($moduloID)){ ?>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                                <?php } ?>
                            </div>
                        </form>
                    </div>
                    <!-- /formulario-editar -->

                </div>
            </div>
        </div>
    </section>
</div>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $tituloModulo ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#"><?= $tituloModulo ?></a></li>
                        <li class="breadcrumb-item active">Editar</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <!-- formulario-editar -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Editar módulo</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="<?=base_url("productos/editarItem/$id")?>" method="post" enctype="multipart/form-data" role="form">
                            <div class="card-body">

                                <!-- descripcion -->
                                <div class="form-group">
                                    <label for="descripcion">Descripción</label>
                                    <input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripción" value="<?=$datosItem->descripcion?>">
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="precio">Precio</label>
                                            <input type="text" class="form-control" name="precio" id="precio" placeholder="Precio del producto" value="<?=$datosItem->precio?>">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                    <div class="form-group">
                                            <label for="tipo">Tipo de producto</label>
                                            <div class="fw">
                                                <select name="tipo" id="tipo" class="select2">
                                                    <option value="---">Seleccionar tipo</option>
                                                    <?php foreach($tiposProductos as $tipo => $desc): $selected = ($tipo === $datosItem->tipo)?'selected="selected"':''; ?>
                                                    <option value="<?=$tipo?>" <?=$selected?> ><?=$desc?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <?php if(isset($datosItem->imagen_1)): ?>
                                            <div class="text-center">
                                                <img src="<?=base_url("assets/media/productos/min/$datosItem->imagen_1")?>" alt="Imagen de producto 1" class="img-thumbnail"><br>
                                                <a href="<?=base_url("productos/eliminarArchivo/1/$id")?>" class="btn btn-danger mt-2" 
                                                   title="Eliminar imagen del producto"
                                                   data-toggle="tooltip" data-placement="right" >
                                                    <span class="fa fa-trash"></span> Eliminar imagen
                                                </a>
                                            </div>
                                            <?php endif; ?>
                                            <div class="custom-file mt-2">
                                            <input type="file" class="custom-file-input" id="imagen_1" name="imagen_1">
                                            <label class="custom-file-label" for="imagen_1">Seleccionar archivo</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <?php if(isset($datosItem->imagen_2)): ?>
                                            <div class="text-center">
                                                <img src="<?=base_url("assets/media/productos/min/$datosItem->imagen_2")?>" alt="Imagen de producto 2" class="img-thumbnail"><br>
                                                <a href="<?=base_url("productos/eliminarArchivo/2/$id")?>" 
                                                   class="btn btn-danger mt-2" 
                                                   title="Eliminar imagen del producto"
                                                   data-toggle="tooltip" data-placement="top" >
                                                    <span class="fa fa-trash"></span> Eliminar imagen
                                                </a>
                                            </div>
                                            <?php endif; ?>
                                            <div class="custom-file mt-2">
                                                <input type="file" class="custom-file-input" id="imagen_2" name="imagen_2">
                                                <label class="custom-file-label" for="imagen_2">Seleccionar archivo</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php $datosItem; ?>

                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <?php if(puedeEditar($moduloID)){ ?>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                                <?php } ?>
                            </div>
                        </form>
                    </div>
                    <!-- /formulario-editar -->

                </div>
            </div>
        </div>
    </section>
</div>
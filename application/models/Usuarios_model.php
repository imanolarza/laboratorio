<?php 
class Usuarios_model extends CI_Model{
    
    var $tabla = '';
    var $moduloID = 0;

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->tabla = 'usuarios';
        $this->moduloID = $this->admin_model->get_modulo_id('usuarios');
    }

    function get_data(){
        $columnas = array();
		$sw = false;
		$sql = sprintf("
            select 
                u.id,
				u.nombre,
                u.apellido,
                u.email,
                u.login,
                t.descripcion as tipo,
				u.activo as acciones
			from
                %s as u,
                tipos_usuarios as t 
			where
				u.tipos_usuarios_id = t.id
				
		",$this->tabla);
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$results = $query->result_array();
			foreach($results as $row){
				$valores = array();
				$id = $row['id'];
				foreach($row as $key => $value){
                    $urlActivar     = base_url("usuarios/activar/$id");
                    $urlDesactivar  = base_url("usuarios/desactivar/$id");
                    $urlEliminar    = base_url("usuarios/eliminar/$id");
                    $urlEditar      = base_url("usuarios/editar/$id");
                    // permisos de botones de acción
                    $puedeActivar = (puedeLeer($this->moduloID));
                    $puedeEditar = (puedeEditar($this->moduloID));
                    $puedeEliminar = (puedeEliminar($this->moduloID));
					switch($key){
                        case 'acciones':
                            
                            if($puedeActivar):
                                if($value == 'S'):
                                    $value = "<a href=\"$urlDesactivar\" class=\"btn btn-primary mr-1\"><span class=\"fa fa-ban\"></span></a>";
                                else:
                                    $value = "<a href=\"$urlActivar\" class=\"btn btn-primary mr-1\"><span class=\"fa fa-check\"></span></a>";
                                endif;
                            else:
                                $value = "";
                            endif;

                            //if($puedeEditar):
                            $value .= "<a href=\"$urlEditar\" class=\"btn btn-dark mr-1\"><span class=\"fa fa-edit\"></span></a>";
                            //endif; 

                            if($puedeEliminar){
                                $value .= "<button type=\"button\" onclick=\"fnCheckEliminar(this)\" data-url=\"$urlEliminar\" class=\"btn btn-danger mr-1\"><span class=\"fa fa-trash\"></span></button>";
                            }

							$value = '<div class="center-block">'.$value.'</div>';
							break;
						default:
							break;
					}
					$valores[]=($value);
					if(!$sw){$columnas[] = array('sTitle'=>$key,'sClass'=>'center');}
				}
				if(!$sw){$sw = true;}
				$data[] = $valores;
			}
		}else{
			$data = array();
		}
		$result_data = array(
			'aaData'=>$data
		);
		return json_encode($result_data);
    }

    function add($postdata){
        /*
        $postdata = array(
            'nombre_campo' => 'valor_campo',
            'nombre_campo' => 'valor_campo',
            'nombre_campo' => 'valor_campo',
        )
        */
        $q = $this->db->insert($this->tabla,$postdata);
        return array( 'status'=>$q, 'id'=>$this->db->insert_id() );
    }

    function edit($id,$postdata){
        $this->db->where('id',$id);
        $q = $this->db->update($this->tabla,$postdata);
        return array( 'status'=>$q );
    }

    function update_pass($hash,$postdata){
        $this->db->where('sha1(email)',$hash);
        $q = $this->db->update($this->tabla,$postdata);
        return array( 'status'=>$q );
    }

    function delete($id){
        $this->delete_permisos_usuario($id);
        $this->db->where('id',$id);
        $q = $this->db->delete($this->tabla);
        return array( 'status'=>$q );
    }

    function get_tipos_usuarios(){
        $result = [];
        $this->db->where('activo','S');
        $query = $this->db->get('tipos_usuarios');
        foreach($query->result() as $row):
            $result[$row->id]=$row->descripcion;
        endforeach;
        return $result;
    }

    function validar_usuario($login,$password){
        $this->db->where('email',$login);
        $this->db->where('pass',sha1($password));
        $query = $this->db->get($this->tabla);
        $datos = [];
        if($query->num_rows() > 0){
            foreach($query->result() as $user):
                $datos = $user;
            endforeach;
            return array('status'=>true,'datos'=>$datos);
        }else{
            return array('status'=>false,'datos'=>$datos);
        }
    }

    function set_estado($id,$valor){
        $this->db->where('id',$id);
        $data = [ 'activo'=>$valor ];
        $query = $this->db->update($this->tabla,$data);
        return $query;
    }

    function get($id){
        $result = [];
        $this->db->where('id',$id);
        $query = $this->db->get($this->tabla);
        foreach($query->result() as $usuario):
            $result = $usuario;
        endforeach;
        return $result;
    }

    function set_permisos_modulo($data){
        $query = $this->db->insert('usuarios_modulos',$data);
        return $query;
    }

    function get_permisos_usuario($uid){
        $r = [];
        $this->db->where('usuarios_id',$uid);
        $query = $this->db->get('usuarios_modulos'); 
        foreach($query->result() as $row):
            $r[$row->modulos_id] = $row;
        endforeach;
        return $r;
    }

    function get_permisos($uid){
        $r = [];
        $this->db->where('usuarios_id',$uid);
        $query = $this->db->get('usuarios_modulos'); 
        foreach($query->result() as $row):
            $r[$row->modulos_id]['leer']        = ($row->leer === 'S');
            $r[$row->modulos_id]['crear']       = ($row->crear === 'S');
            $r[$row->modulos_id]['editar']      = ($row->editar === 'S');
            $r[$row->modulos_id]['eliminar']    = ($row->eliminar === 'S');
        endforeach;
        return $r;
    }

    function delete_permisos_usuario($uid){
        $this->db->where('usuarios_id',$uid);
        $query = $this->db->delete('usuarios_modulos');
        return $query;
    }

    function existe_email_usuario($email){
        $this->db->where('email',$email);
        $query = $this->db->get($this->tabla);
        return ($query->num_rows() > 0);
    }

    function validar_hash($hash){
        $this->db->where('sha1(email)',$hash);
        $query = $this->db->get($this->tabla);
        return ($query->num_rows() > 0);
    }

    function buscar_usuario($query){
        $r = [];
        $sql = "select * from usuarios where ( cedula like '%$query%' or concat(nombre,' ',apellido) like '%$query%')";
        $q = $this->db->query($sql);
        foreach($q->result() as $row):
            $r[] = [
                'id' => $row->id,
                'text' => $row->nombre . " " . $row->apellido
            ];
        endforeach;
        return array('items'=>$r,'total_count'=>$this->get_total_items());
    }

    function get_total_items(){
		$result = 0;
		$sql = "select count(*) total from usuarios";
		$query = $this->db->query($sql);
		foreach($query->result() as $row){
			$result = $row->total;
		}
		return $result;
	}

}
<?php 
class Upload_model extends CI_Model{
    
    var $tabla = '';

    public function __construct() {
        parent::__construct();
    }

    function subir_img_producto($file,$modulo)
	{
		$data = array();
		$status = array();
		$this->img_path = realpath(APPPATH . '../assets/media/'.$modulo.'/');
		$foo = new Verot\Upload\Upload($file);
		$file_name = uniqid();
		if ($foo->uploaded) {
			// ---> GENERAMOS EL THUMBNAIL
			$foo->file_new_name_body	= 'sc_'.$file_name;
			$foo->image_resize         	= true;
			$foo->image_ratio          	= true;
			$foo->image_ratio_crop     	= true;
			$foo->image_x              	= 150;
			$foo->image_y              	= 150;
			$foo->process(sprintf('%s/min/',$this->img_path));
			$status[] = ($foo->processed)?'ok':$foo->error;
			
			// ---> GENERAMOS EL SLIDER
			$foo->file_new_name_body	= 'sc_'.$file_name;
			$foo->image_resize         	= true;
			$foo->image_ratio          	= true;
			$foo->image_ratio_crop     	= true;
			$foo->image_x              	= 400;
			$foo->image_y              	= 400;
			$foo->process(sprintf('%s/',$this->img_path));
			$status[] = ($foo->processed)?'ok':$foo->error;
			
			$data = array('status' 	=> true,
						  'error' 	=> $status,
						  'file' 	=> $foo->file_dst_name);
		}else{
			$status[] = $foo->error;
			$data = array('status' 	=> false,
						  'error' 	=> $status,
						  'data'	=> $this->img_path.'/'.$file);
		}
		$foo->clean();
		return $data;
	}

	function eliminar_img($fileName,$modulo){
		$this->removeFiles($modulo,$fileName);
	}

	function removeFiles($modulo,$file)
	{
		$path = realpath(str_replace("\\", "/", APPPATH). sprintf('../assets/media/%s/',$modulo));
		$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
		foreach($objects as $name => $object){
			$filepath = explode('/',$name);
			if($file == $filepath[count($filepath)-1]){
				unlink($name);
			}
		}
	}

	function fileArray()
	{
		$files = array();
		if(count($_FILES) > 0){
			foreach($_FILES as $f){
				$i=0;
				if(is_array($f['name'])){
					foreach($f['name'] as $v){
						$ext = explode('/',$f['type'][$i]);
						$files[] = array(
							'name'		=> $v,
							'type'		=> $f['type'][$i],
							'tmp_name'	=> $f['tmp_name'][$i],
							'error'		=> $f['error'][$i],
							'size'		=> $f['size'][$i],
							'mime'		=> (isset($ext[1]))?$ext[1]:NULL
						);
						$i++;
					}
				}
			} // ---> end files each
		}
		return $files;
	}



}
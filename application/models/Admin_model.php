<?php 
class Admin_model extends CI_Model{
    
    var $tabla = '';

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->tabla = 'modulos';
    }

    function get_modulos(){
        $res = [];
        $this->db->where('activo','S');
        $query = $this->db->get('modulos');
        foreach($query->result() as $row):
            $res[]=$row;
        endforeach;
        return $res;
    }

    function get_modulo_id($nombreModulo){
        $res = 0;
        $this->db->where('lower(descripcion)',strtolower($nombreModulo));
        $query = $this->db->get('modulos');
        foreach($query->result() as $row):
            $res=$row->id;
        endforeach;
        return $res;
    }

    function get_tipo_usuario($uid){
        $r = "";
        $sql = "
        select 
            t.descripcion as tipo 
        from 
            tipos_usuarios t, 
            usuarios u 
        where 
            t.id = u.tipos_usuarios_id
            and u.id = $uid
        ";
        $q = $this->db->query($sql);
        foreach($q->result() as $row):
            $r = strtoupper($row->tipo);
        endforeach;
        return $r;
    }

}
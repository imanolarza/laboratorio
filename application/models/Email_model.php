<?php 
class Email_model extends CI_Model{
    
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('mailer');
    }

    function send_mail($to,$data,$msg)
	{
		$bcc = array(
			'Development' => 'carlos.martinez.web@gmail.com'
		);
		try{
			/*
			usuario	: 3bti@ctj.edu.py
			password: 3btiMail2020
			*/
			$mail = new PHPMailer\PHPMailer\PHPMailer(TRUE);
			$body = $msg;
			$mail->IsSMTP();
			$mail->SMTPAuth   	= true;						
			$mail->SMTPSecure 	= "ssl";
			$mail->Host 		= "nippon.websitewelcome.com";
			$mail->Username 	= "3bti@ctj.edu.py";
			$mail->Password 	= "3btiMail2020";
			$mail->Port 		= 465;
			$mail->From       	= "3bti@ctj.edu.py";
			$mail->FromName   	= $data->from;
			$mail->Subject		= $data->subject;
			$mail->AddAddress($to,$data->name);
			foreach($bcc as $key => $value){ $mail->AddBCC($value,$key); }
			$mail->MsgHTML($body);
			$mail->IsHTML(true);
			$mail->Send();
			$result	= array(
				'status'	=> TRUE,
				'e'			=> 'Notificado'
			);
		}catch ( PHPMailer\PHPMailer\Exception $e) {
			$result = array(
				'status'	=> FALSE,
				'e'			=> $e->errorMessage()
			);
		}
		return $result;
	}

}
<?php
class Pedidos_model extends CI_Model
{

    var $tabla = '';
    var $tabla_det = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->tabla        = 'pedidos_cab';
        $this->tabla_det    = 'pedidos_det';
    }

    function get_data()
    {
        $columnas = array();
        $sw = false;
        $tipoUsuario = getTipoUsuario(getUID());
        switch ($tipoUsuario):
            case 'ALUMNO':
            case 'PROFESOR':
                $uid = getUID();
                $sql = sprintf("
                    select 
                        p.id,
                        p.fecha_hora as fecha,
                        concat(u.nombre,' ',u.apellido) as usuario,
                        p.estados as detalle,
                        p.monto,
                        p.estados as estado,
                        p.estados as acciones
                    from
                        %s as p,
                        usuarios as u
                    where 
                        p.usuarios_id = u.id
                        and u.id = $uid
                    order by p.id desc
                ", $this->tabla);
                break;
            default:
                $sql = sprintf("
                    select 
                        p.id,
                        p.fecha_hora as fecha,
                        concat(u.nombre,' ',u.apellido) as usuario,
                        p.estados as detalle,
                        p.monto,
                        p.estados as estado,
                        p.estados as acciones
                    from
                        %s as p,
                        usuarios as u
                    where 
                        p.usuarios_id = u.id
                    order by p.id desc
                ", $this->tabla);
                break;
        endswitch;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $results = $query->result_array();
            foreach ($results as $row) {
                $valores = array();
                $id = $row['id'];
                $estado = $row['estado'];
                foreach ($row as $key => $value) {
                    $urlConfirmar   = base_url("pedidos/entregar/$id");
                    $urlCancelar    = base_url("pedidos/cancelar/$id");
                    $urlEliminar    = base_url("pedidos/eliminar/$id");
                    $urlVer         = base_url("pedidos/ver/$id");
                    switch ($key) {
                        case 'acciones':
                            if ($estado === 'E') :
                                $value = "<a href=\"#\" class=\"btn btn-primary mr-1\"><span class=\"fa fa-check\"></span></a>";
                            elseif ($estado === 'C') :
                                $value = "<a href=\"#\" class=\"btn btn-warning mr-1\"><span class=\"fa fa-ban\"></span></a>";
                                $value .= "<a href=\"$urlEliminar\" class=\"btn btn-danger mr-1\"><span class=\"fa fa-trash\"></span></a>";
                            else :
                                if ($tipoUsuario == 'CANTINA' || $tipoUsuario == 'ADMINISTRADOR') {
                                    $value = "<a href=\"$urlConfirmar\" class=\"btn btn-success mr-1\"><span class=\"fa fa-check\"></span></a>";
                                    $value .= "<a href=\"$urlCancelar\" class=\"btn btn-warning mr-1\"><span class=\"fa fa-ban\"></span></a>";
                                } else {
                                    $value = "<a href=\"$urlCancelar\" class=\"btn btn-warning mr-1\"><span class=\"fa fa-ban\"></span></span></a>";
                                }
                                $value .= "<button type=\"button\" onclick=\"fnCheckEliminar(this)\" data-url=\"$urlEliminar\" class=\"btn btn-danger mr-1\"><span class=\"fa fa-trash\"></span></button>";
                            endif;
                            $value .= "<a href=\"$urlVer\" class=\"btn btn-info mr-1\"><span class=\"fa fa-eye\"></span></a>";
                            $value = '<div class="center-block">' . $value . '</div>';
                            break;
                        case 'detalle':
                            $tagsDetalle = $this->generar_detalle_pedido($id);
                            $value = implode('<br>', $tagsDetalle);
                            break;
                        case 'monto':
                            $value = number_format($value, 0, ',', '.');
                            break;
                        case 'estado':
                            $value = $this->generar_tag_estado($value);
                            break;
                        default:
                            break;
                    }
                    $valores[] = ($value);
                    if (!$sw) {
                        $columnas[] = array('sTitle' => $key, 'sClass' => 'center');
                    }
                }
                if (!$sw) {
                    $sw = true;
                }
                $data[] = $valores;
            }
        } else {
            $data = array();
        }
        $result_data = array(
            'aaData' => $data
        );
        return json_encode($result_data);
    }

    function get_data_filtrada($desde = false,$hasta = false)
    {
        $columnas = array();
        $sw = false;
        $tipoUsuario = getTipoUsuario(getUID());
        $hoy = date('Y-m-d');
        $filtroFecha = "and p.fecha_pedido = '$hoy'";
        if($desde){
            if($hasta){
                $fDesde = formatoFechaDB($desde);
                $fHasta = formatoFechaDB($hasta);
                $filtroFecha = "and p.fecha_pedido between '$fDesde' and '$fHasta'";
            }else{
                $fDesde = formatoFechaDB($desde);
                $filtroFecha = "and p.fecha_pedido >= '$fDesde'";
            }
        }
        switch ($tipoUsuario):
            case 'ALUMNO':
            case 'PROFESOR':
                $uid = getUID();
                $sql = "
                    select 
                        p.id,
                        p.fecha_hora as fecha,
                        concat(u.nombre,' ',u.apellido) as usuario,
                        p.estados as detalle,
                        p.monto,
                        p.estados as estado,
                        p.estados as acciones
                    from
                        $this->tabla as p,
                        usuarios as u
                    where 
                        p.usuarios_id = u.id
                        and u.id = $uid
                        $filtroFecha
                    order by p.id desc
                ";
                break;
            default:
                $sql = "
                    select 
                        p.id,
                        date_format(p.fecha_pedido,'%d/%m/%Y') as fecha,
                        concat(u.nombre,' ',u.apellido) as usuario,
                        p.estados as detalle,
                        p.monto,
                        p.estados as estado,
                        p.estados as acciones
                    from
                        $this->tabla as p,
                        usuarios as u
                    where 
                        p.usuarios_id = u.id
                        $filtroFecha
                    order by p.id desc
                ";
                break;
        endswitch;
        //echo $sql;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $results = $query->result_array();
            foreach ($results as $row) {
                $valores = array();
                $id = $row['id'];
                $estado = $row['estado'];
                foreach ($row as $key => $value) {
                    $urlConfirmar   = base_url("pedidos/entregar/$id");
                    $urlCancelar    = base_url("pedidos/cancelar/$id");
                    $urlEliminar    = base_url("pedidos/eliminar/$id");
                    $urlVer         = base_url("pedidos/ver/$id");
                    switch ($key) {
                        case 'acciones':
                            if ($estado === 'E') :
                                $value = "<a href=\"#\" class=\"btn btn-primary mr-1\"><span class=\"fa fa-check\"></span></a>";
                            elseif ($estado === 'C') :
                                $value = "<a href=\"#\" class=\"btn btn-warning mr-1\"><span class=\"fa fa-ban\"></span></a>";
                                $value .= "<a href=\"$urlEliminar\" class=\"btn btn-danger mr-1\"><span class=\"fa fa-trash\"></span></a>";
                            else :
                                if ($tipoUsuario == 'CANTINA' || $tipoUsuario == 'ADMINISTRADOR') {
                                    $value = "<a href=\"$urlConfirmar\" class=\"btn btn-success mr-1\"><span class=\"fa fa-check\"></span></a>";
                                    $value .= "<a href=\"$urlCancelar\" class=\"btn btn-warning mr-1\"><span class=\"fa fa-ban\"></span></a>";
                                } else {
                                    $value = "<a href=\"$urlCancelar\" class=\"btn btn-warning mr-1\"><span class=\"fa fa-ban\"></span></span></a>";
                                }
                                $value .= "<button type=\"button\" onclick=\"fnCheckEliminar(this)\" data-url=\"$urlEliminar\" class=\"btn btn-danger mr-1\"><span class=\"fa fa-trash\"></span></button>";
                            endif;
                            $value .= "<a href=\"$urlVer\" class=\"btn btn-info mr-1\"><span class=\"fa fa-eye\"></span></a>";
                            $value = '<div class="center-block">' . $value . '</div>';
                            break;
                        case 'detalle':
                            $tagsDetalle = $this->generar_detalle_pedido($id);
                            $value = implode('<br>', $tagsDetalle);
                            break;
                        case 'monto':
                            $value = number_format($value, 0, ',', '.');
                            break;
                        case 'estado':
                            $value = $this->generar_tag_estado($value);
                            break;
                        default:
                            break;
                    }
                    $valores[] = ($value);
                    if (!$sw) {
                        $columnas[] = array('sTitle' => $key, 'sClass' => 'center');
                    }
                }
                if (!$sw) {
                    $sw = true;
                }
                $data[] = $valores;
            }
        } else {
            $data = array();
        }
        $result_data = array(
            'aaData' => $data
        );
        return json_encode($result_data);
    }

    function add($postdata)
    {
        $q = $this->db->insert($this->tabla, $postdata);
        return array('status' => $q, 'id' => $this->db->insert_id());
    }

    function add_cabecera($postdata)
    {
        $q = $this->db->insert($this->tabla, $postdata);
        return array('status' => $q, 'id' => $this->db->insert_id());
    }

    function add_detalle($postdata)
    {
        $q = $this->db->insert($this->tabla_det, $postdata);
        return array('status' => $q, 'id' => $this->db->insert_id());
    }

    function edit($id, $postdata)
    {
        $this->db->where('id', $id);
        $q = $this->db->update($this->tabla, $postdata);
        return array('status' => $q);
    }

    function delete($id)
    {
        // eliminamos el detalle del pedido
        $this->delete_detalle($id);
        // eliminamos el pedido
        $this->db->where('id', $id);
        $q = $this->db->delete($this->tabla);
        return array('status' => $q);
    }

    function delete_detalle($pid)
    {
        $this->db->where('pedidos_cab_id', $pid);
        $q = $this->db->delete($this->tabla_det);
        return $q;
    }

    function get_tipos_usuarios()
    {
        $result = [];
        $this->db->where('activo', 'S');
        $query = $this->db->get('tipos_usuarios');
        foreach ($query->result() as $row) :
            $result[$row->id] = $row->descripcion;
        endforeach;
        return $result;
    }

    function set_estado($id, $valor)
    {
        $this->db->where('id', $id);
        $data = ['estados' => $valor];
        $query = $this->db->update($this->tabla, $data);
        return $query;
    }

    function get($id)
    {
        $result = [];
        $this->db->where('id', $id);
        $query = $this->db->get($this->tabla);
        foreach ($query->result() as $registro) :
            $result = $registro;
        endforeach;
        return $result;
    }

    function get_tipo_usuario($tipoUsuarioID)
    {
        $result = '---';
        $this->db->where('id', $tipoUsuarioID);
        $query = $this->db->get($this->tabla);
        foreach ($query->result() as $row) :
            $result = $row->descripcion;
        endforeach;
        return $result;
    }

    function generar_detalle_pedido($pid)
    {
        $r = [];
        $sql = "
        select 
            p.descripcion as producto, 
            d.cantidad as cantidad, 
            d.subtotal 
        from 
            producto p, 
            pedidos_det d 
        where 
            p.id = d.producto_id 
            and d.pedidos_cab_id = $pid
        ";
        $q = $this->db->query($sql);
        foreach ($q->result() as $row) :
            $r[] = sprintf(
                "<span class=\"badge badge-primary\">%s (%s) - %s</span>",
                $row->producto,
                $row->cantidad,
                number_format($row->subtotal, 0, ',', '.')
            );
        endforeach;
        return $r;
    }

    function generar_detalle_pedido_reporte($pid)
    {
        $r = [];
        $sql = "
        select 
            p.descripcion as producto, 
            d.cantidad as cantidad, 
            d.subtotal 
        from 
            producto p, 
            pedidos_det d 
        where 
            p.id = d.producto_id 
            and d.pedidos_cab_id = $pid
        ";
        $q = $this->db->query($sql);
        foreach ($q->result() as $row) :
            $r[] = sprintf(
                "%s (%s) | %s",
                $row->producto,
                $row->cantidad,
                number_format($row->subtotal, 0, ',', '.')
            );
        endforeach;
        return $r;
    }

    function generar_tag_estado($estado)
    {
        $r = '';
        switch ($estado) {
            case 'P':
                $r = "<span class=\"badge badge-warning\">PENDIENTE</span>";
                break;
            case 'C':
                $r = "<span class=\"badge badge-danger\">CANCELADO</span>";
                break;
            case 'E':
                $r = "<span class=\"badge badge-success\">ENTREGADO</span>";
                break;
        }
        return $r;
    }

    function get_pedidos_estado($e = false)
    {
        $res = 0;
        $hoy = date('Y-m-d');
        if ($e) :
            $sql = "select count(*) pedidos from pedidos_cab where fecha_pedido = '$hoy' and estados = '$e'";
        else :
            $sql = "select count(*) pedidos from pedidos_cab where fecha_pedido = '$hoy'";
        endif;
        $q = $this->db->query($sql);
        foreach ($q->result() as $row) :
            $res = $row->pedidos;
        endforeach;
        return $res;
    }

    function get_productos_pedido($pid)
    {
        $r = [];
        $sql = "select p.* from producto p, pedidos_det d where p.id = d.producto_id and d.pedidos_cab_id = $pid";
        $q = $this->db->query($sql);
        foreach ($q->result() as $row) :
            $r[] = $row;
        endforeach;
        return $r;
    }
}

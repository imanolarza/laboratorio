<?php 
class Tipos_usuarios_model extends CI_Model{
    
    var $tabla = '';

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->tabla = 'tipos_usuarios';
    }

    function get_data(){
        $columnas = array();
		$sw = false;
		$sql = sprintf("
            select 
                u.id,
				u.descripcion,
                u.activo as acciones
			from
                %s as u
				
		",$this->tabla);
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$results = $query->result_array();
			foreach($results as $row){
				$valores = array();
				$id = $row['id'];
				foreach($row as $key => $value){
                    $urlActivar     = base_url("tiposUsuarios/activar/$id");
                    $urlDesactivar  = base_url("tiposUsuarios/desactivar/$id");
                    $urlEliminar    = base_url("tiposUsuarios/eliminar/$id");
                    $urlEditar      = base_url("tiposUsuarios/editar/$id");
					switch($key){
						case 'acciones':
							if($value == 'S'):
								$value = "<a href=\"$urlDesactivar\" class=\"btn btn-primary mr-1\"><span class=\"fa fa-ban\"></span></a>";
                            else:
								$value = "<a href=\"$urlActivar\" class=\"btn btn-primary mr-1\"><span class=\"fa fa-check\"></span></a>";
							endif;
							$value .= "<a href=\"$urlEditar\" class=\"btn btn-dark mr-1\"><span class=\"fa fa-edit\"></span></a>";
							$value .= "<button type=\"button\" onclick=\"fnCheckEliminar(this)\" data-url=\"$urlEliminar\" class=\"btn btn-danger mr-1\"><span class=\"fa fa-trash\"></span></button>";
							$value = '<div class="center-block">'.$value.'</div>';
							break;
						default:
							break;
					}
					$valores[]=($value);
					if(!$sw){$columnas[] = array('sTitle'=>$key,'sClass'=>'center');}
				}
				if(!$sw){$sw = true;}
				$data[] = $valores;
			}
		}else{
			$data = array();
		}
		$result_data = array(
			'aaData'=>$data
		);
		return json_encode($result_data);
    }

    function add($postdata){
        $q = $this->db->insert($this->tabla,$postdata);
        return array( 'status'=>$q, 'id'=>$this->db->insert_id() );
    }

    function edit($id,$postdata){
        $this->db->where('id',$id);
        $q = $this->db->update($this->tabla,$postdata);
        return array( 'status'=>$q );
    }

    function delete($id){
        $this->db->where('id',$id);
        $q = $this->db->delete($this->tabla);
        return array( 'status'=>$q );
    }

    function get_tipos_usuarios(){
        $result = [];
        $this->db->where('activo','S');
        $query = $this->db->get('tipos_usuarios');
        foreach($query->result() as $row):
            $result[$row->id]=$row->descripcion;
        endforeach;
        return $result;
    }

    function set_estado($id,$valor){
        $this->db->where('id',$id);
        $data = [ 'activo'=>$valor ];
        $query = $this->db->update($this->tabla,$data);
        return $query;
    }

    function get($id){
        $result = [];
        $this->db->where('id',$id);
        $query = $this->db->get($this->tabla);
        foreach($query->result() as $registro):
            $result = $registro;
        endforeach;
        return $result;
    }

    function get_tipo_usuario($tipoUsuarioID){
        $result = '---';
        $this->db->where('id',$tipoUsuarioID);
        $query = $this->db->get($this->tabla);
        foreach($query->result() as $row):
            $result = $row->descripcion;
        endforeach;
        return $result;
    }

}
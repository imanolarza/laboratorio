<?php 
class Reportes_model extends CI_Model{
    
    var $tabla = '';

    public function __construct() {
        parent::__construct();
        $this->load->model('pedidos_model');
        $this->load->database();
        $this->tabla = '';
    }

    function get_pedidos_rango_fecha($fechaDesde,$fechaHasta){
        $data = [];
        $desde = formatoFechaDB($fechaDesde);
        $hasta = formatoFechaDB($fechaHasta);
        $uid = getUID();
        $tipoUsuario = getTipoUsuario(getUID());
        $filtroTipoUsuario = ($tipoUsuario == 'CANTINA' || $tipoUsuario == 'ADMINISTRADOR')?
            '':
            "and p.usuarios_id = $uid";
        $sql = "select 
            p.id,
            p.fecha_hora,
            p.fecha_pedido as fecha,
            concat(u.nombre,' ',u.apellido) as usuario,
            p.estados as detalle,
            p.monto as total,
            p.estados as estado
        from
            pedidos_cab as p,
            usuarios as u
        where 
            p.usuarios_id = u.id
            and p.fecha_pedido between '$desde' and '$hasta' 
            $filtroTipoUsuario
        order by p.fecha_pedido asc";
        //echo $sql;
        $q = $this->db->query($sql);
        $results = $q->result_array();
        foreach ($results as $row) {
            $valores = [];
            $id = $row['id'];
            foreach ($row as $key => $value) {
                switch ($key) {
                    case 'detalle':
                        $tagsDetalle = $this->pedidos_model->generar_detalle_pedido($id);
                        $valores[$key] = implode('<br>', $tagsDetalle);
                        break;
                    case 'total':
                        $valores[$key] = number_format($value, 0, ',', '.');
                        break;
                    case 'estado':
                        $valores[$key] = $this->pedidos_model->generar_tag_estado($value);
                        break;
                    default:
                        $valores[$key] = $value;
                        break;
                }
            }
            $data[] = $valores;
        }
        return $data;
    }

    function reporte_pedidos_rango_fecha($fechaDesde,$fechaHasta){
        $data = [];
        $desde = formatoFechaDB($fechaDesde);
        $hasta = formatoFechaDB($fechaHasta);
        $uid = getUID();
        $tipoUsuario = getTipoUsuario(getUID());
        $filtroTipoUsuario = ($tipoUsuario == 'CANTINA' || $tipoUsuario == 'ADMINISTRADOR')?
            '':
            "and p.usuarios_id = $uid";
        $sql = "select 
            p.id,
            p.fecha_hora,
            p.fecha_pedido as fecha,
            concat(u.nombre,' ',u.apellido) as usuario,
            p.estados as detalle,
            p.monto as total,
            p.estados as estado
        from
            pedidos_cab as p,
            usuarios as u
        where 
            p.usuarios_id = u.id
            and p.fecha_pedido between '$desde' and '$hasta' 
            $filtroTipoUsuario
        order by p.fecha_pedido asc";
        //echo $sql;
        $q = $this->db->query($sql);
        $results = $q->result_array();
        $columnasCargadas = false;
        $columnas = [];
        foreach ($results as $row) {
            $valores = [];
            $id = $row['id'];
            foreach ($row as $key => $value) {
                switch ($key) {
                    case 'detalle':
                        $tagsDetalle = $this->pedidos_model->generar_detalle_pedido_reporte($id);
                        $valores[$key] = implode(' - ', $tagsDetalle);
                        break;
                    case 'total'    : $valores[$key] = number_format($value, 0, ',', '.'); break;
                    case 'estado'   : $valores[$key] = $this->generar_tag_estado($value); break;
                    case 'usuario'  : $valores[$key] = $value; break;
                    default         : $valores[$key] = $value; break;
                }
                if(!$columnasCargadas){
                    $columnas[]=$key;
                }
            }
            $data[] = $valores;
            $columnasCargadas = true;
        }
        return array('columnas'=>$columnas,'datos'=>$data);
    }

    function generar_tag_estado($estado)
    {
        $r = '';
        switch ($estado) {
            case 'P':
                $r = "PENDIENTE";
                break;
            case 'C':
                $r = "CANCELADO";
                break;
            case 'E':
                $r = "ENTREGADO";
                break;
        }
        return $r;
    }

}
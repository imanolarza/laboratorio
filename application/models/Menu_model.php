<?php 
class Menu_model extends CI_Model{
    
    var $tabla = '';

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->tabla = 'menu';
    }

    function get_data(){
        $columnas = array();
		$sw = false;
		$sql = sprintf("
            select 
                u.id,
                u.descripcion,
                u.activo as dias,
                u.activo as acciones,
                u.lu,
                u.ma,
                u.mi,
                u.ju,
                u.vi
			from
                %s as u
				
		",$this->tabla);
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$results = $query->result_array();
			foreach($results as $row){
				$valores = array();
                $id = $row['id'];
                $dias = "";
                $idx = 0;
                $posDias = 0;
				foreach($row as $key => $value){
                    $urlActivar     = base_url("menu/activar/$id");
                    $urlDesactivar  = base_url("menu/desactivar/$id");
                    $urlEliminar    = base_url("menu/eliminar/$id");
                    $urlEditar      = base_url("menu/editar/$id");
					switch($key){
						case 'acciones':
							if($value == 'S'):
								$value = "<a href=\"$urlDesactivar\" class=\"btn btn-primary mr-1\"><span class=\"fa fa-ban\"</a>";
                            else:
								$value = "<a href=\"$urlActivar\" class=\"btn btn-primary mr-1\"><span class=\"fa fa-check\"</a>";
							endif;
							$value .= "<a href=\"$urlEditar\" class=\"btn btn-dark mr-1\"><span class=\"fa fa-edit\"</span></a>";
                            $value .= "<button type=\"button\" onclick=\"fnCheckEliminar(this)\" data-url=\"$urlEliminar\" class=\"btn btn-danger mr-1\"><span class=\"fa fa-trash\"></span></button>";
							$value = '<div class="center-block">'.$value.'</div>';
                            break;
                        case 'lu': if($value == 'S'){ $dias .= generarTag(strtoupper($key)); } break;
                        case 'ma': if($value == 'S'){ $dias .= generarTag(strtoupper($key)); } break;
                        case 'mi': if($value == 'S'){ $dias .= generarTag(strtoupper($key)); } break;
                        case 'ju': if($value == 'S'){ $dias .= generarTag(strtoupper($key)); } break;
                        case 'vi': if($value == 'S'){ $dias .= generarTag(strtoupper($key)); } break;
                        case 'dias': $posDias = $idx; break;
						default: break;
					}
					$valores[]=($value);
                    if(!$sw){$columnas[] = array('sTitle'=>$key,'sClass'=>'center');}
                    $idx++;
				}
                if(!$sw){$sw = true;}
                $valores[$posDias] = $dias;
				$data[] = $valores;
			}
		}else{
			$data = array();
		}
		$result_data = array(
			'aaData'=>$data
		);
		return json_encode($result_data);
    }

    function add($postdata){
        $q = $this->db->insert($this->tabla,$postdata);
        return array( 'status'=>$q, 'id'=>$this->db->insert_id() );
    }

    function edit($id,$postdata){
        $this->db->where('id',$id);
        $q = $this->db->update($this->tabla,$postdata);
        return array( 'status'=>$q );
    }

    function delete($id){

        // eliminamos los productos asociados al menu
        $this->delete_productos_menu($id);
        // eliminamos el menu
        $this->db->where('id',$id);
        $q = $this->db->delete($this->tabla);
        return array( 'status'=>$q );
    }

    function delete_productos_menu($mid){
        $this->db->where('menu_id',$mid);
        $q = $this->db->delete('menu_producto');
        return $q;
    }

    function get($id){
        $result = [];
        $this->db->where('id',$id);
        $query = $this->db->get($this->tabla);
        foreach($query->result() as $registro):
            $result = $registro;
        endforeach;
        return $result;
    }

    function set_estado($id,$valor){
        $this->db->where('id',$id);
        $data = [ 'activo'=>$valor ];
        $query = $this->db->update($this->tabla,$data);
        return $query;
    }

    function get_menus(){
        $result = [];
        $arrDias = unserialize(DIAS_MENU);
        $diaSemana = date('w'); //0-6 
        $paramDiaSemana = $arrDias[($diaSemana < 1)?$diaSemana:($diaSemana-1)];
        $this->db->where($paramDiaSemana,'S');
        $query = $this->db->get($this->tabla);
        // $sql = "select * from menu where $paramDiaSemana = 'S'";
        // $query = $this->db->query($sql);
        // echo $sql;
        foreach($query->result() as $row):
            $result[$row->id]=$row->descripcion;
        endforeach;
        return $result;
    }
    
    function get_menu($menuID){
        $result = '---';
        $this->db->where('id',$menuID);
        $query = $this->db->get($this->tabla);
        foreach($query->result() as $row):
            $result = $row->descripcion;
        endforeach;
        return $result;
    }

    function add_producto_menu($mid,$pid){
        $itemFormData = [
            'menu_id'       => $mid,
            'producto_id'   => $pid
        ];
        $query = $this->db->insert('menu_producto',$itemFormData);
        return $query;
    }

    function get_productos_menu($mid){
        $r = [];
        $this->db->where('menu_id',$mid);
        $q = $this->db->get('menu_producto');
        foreach($q->result() as $row):
            $r[]=$row->producto_id;
        endforeach;
        return $r;
    }

}
fnCheckEliminar = function(elem){
    console.log("url eliminar item:"+$(elem).data('url'));
    Swal.fire({
        title: 'El item será eliminado, ¿está seguro?',
        icon:'warning',
        showCancelButton: true,
        confirmButtonText: 'Eliminar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value) {
            var urlRedireccion = $(elem).data('url');
            window.location.replace(urlRedireccion);
        }
    });
};